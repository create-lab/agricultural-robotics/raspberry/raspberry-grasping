from time import sleep, time
import numpy as np
import sys

from yaml import load
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
import json
import socket
from Control import Controller


sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871


loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
loadcell.connect_and_handshake()

# Define what data to send to plotjuggler
newData = {
    "zero":0
}

while True:
        loadcell.receive_message()

        if loadcell.newMsgReceived:
            compression_force = np.abs(float(loadcell.receivedMessages['lc2']))

            newData["gripping force"] = compression_force

            sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 
