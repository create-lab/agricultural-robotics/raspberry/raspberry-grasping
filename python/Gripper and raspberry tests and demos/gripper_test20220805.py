from time import sleep, time
import numpy as np
import sys

from yaml import load
sys.path.append('libraries/')

# from libraries.dynamixel_controller import dynamixel
# from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
print("\n\n\n\n\n\n")


# open_position = 1400 # 123.22º
# close_position = 3600 # 316.32º

# gripper = dynamixel(ID=2, descriptive_device_name="Gripper", 
#                     series_name="xl", baudrate=1000000, port_name="COM4")


# gripper.begin_communication()
# gripper.set_operating_mode("position")

# gripper.write_position(open_position)
# sleep(1)
# position_read = gripper.read_position()
# while position_read < close_position:
#     position = position_read + 100
#     gripper.write_position(position)
#     position_read = gripper.read_position()


loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
#loadcell.define_number_of_messages(2, 0)
loadcell.connect_and_handshake()

counter = 0
timer = time()
while 1:
    loadcell.receive_message(True)

    if loadcell.newMsgReceived:
        print(loadcell.receivedMessages)
    sleep(0.1)
    counter += 1

    if counter == 30:
        loadcell.send_message("read_d")
