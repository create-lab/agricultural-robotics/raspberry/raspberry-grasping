import numpy as np
import cv2 as cv
from cv_helpers import *
import matplotlib.pyplot as plt
import depthai as dai
from gen2_calc_spatials_on_host.calc import HostSpatialsCalc
from gen2_calc_spatials_on_host.utility import *
import math

# GOOD PLACEMENT OF THE CAMERA: about 545mm away

# Tunable parametres
# RGB window
w_rgb     = 640
h_rgb     = 400
# Camera offset
y_init    = 60
x_init    = 55
h_adj     = 300
w_adj     = 480
scale     = w_adj/w_rgb
# Blur kernel
n_blur    = 9
# Hue threshold
thresh    = 160 
# CHT Parameters
minDist   = 30
param1    = 100
param2    = 22
minRadius = 1
maxRadius = 50
# Define how many frames will be analysed and the initial delay 
delay = 150
cameraOn = 400

# Create pipeline
pipeline = dai.Pipeline()

# Define source and output
camRgb = pipeline.create(dai.node.ColorCamera)
monoLeft = pipeline.create(dai.node.MonoCamera)
monoRight = pipeline.create(dai.node.MonoCamera)
stereo = pipeline.create(dai.node.StereoDepth)

# Properties
camRgb.setPreviewSize(w_rgb, h_rgb)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

stereo.initialConfig.setConfidenceThreshold(255)
stereo.setLeftRightCheck(False)
stereo.setSubpixel(True)
stereo.setExtendedDisparity(False)

# Linking
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)

xoutRgb = pipeline.create(dai.node.XLinkOut)
xoutRgb.setStreamName("rgb")
camRgb.preview.link(xoutRgb.input)

xoutDepth = pipeline.create(dai.node.XLinkOut)
xoutDepth.setStreamName("depth")
stereo.depth.link(xoutDepth.input)

xoutDepth = pipeline.create(dai.node.XLinkOut)
xoutDepth.setStreamName("disp")
stereo.disparity.link(xoutDepth.input)


# Connect to device and start pipeline
with dai.Device(pipeline) as device:

   # Output queue will be used to get the rgb frames from the output defined above
    qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
    depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
    dispQ = device.getOutputQueue(name="disp")

    hostSpatials = HostSpatialsCalc(device)
    text = TextHelper()
    delta = 5
    hostSpatials.setDeltaRoi(delta)

    depth4precision = []
    depth_coord = []

    num_rasp = 0

    # to measure precision
    sum_x = 0
    sum_y = 0
    sum_z = 0
    all_x = []
    all_y = []
    all_z = []

    while cameraOn > 0:
        image  = qRgb.get().getCvFrame() # blocking call, will wait until a new data has arrived

        image = rescaleFrame(image, scale)

        image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        image_gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY) 

        hue = image_hsv[:,:,0] 
        new_hue = np.zeros_like(hue)
        new_hue[np.where(hue>thresh)] = image_gray[np.where(hue>thresh)]# + bias

        blur_rasps = cv.GaussianBlur(new_hue,(n_blur,n_blur), 0)

        rasp_coords, rasp_windows, found_rasps = find_circles(image, blur_rasps, minDist, param1, param2, minRadius, maxRadius)

        depthFrame = depthQueue.get().getFrame() 
        depthFrame = depthFrame[y_init:y_init+h_adj, x_init:x_init+w_adj]

        # Get disparity frame for nicer depth visualization
        disp = dispQ.get().getFrame()
        disp = (disp * (255 / stereo.initialConfig.getMaxDisparity())).astype(np.uint8)
        disp = cv.applyColorMap(disp, cv.COLORMAP_JET)
        disp = disp[y_init:y_init+h_adj, x_init:x_init+w_adj]

        for i, bbox in enumerate(rasp_windows):
           
            # Calculate spatial coordiantes from depth frame
            (x, y) = (rasp_coords[i][0],rasp_coords[i][1])
            spatials, centroid = hostSpatials.calc_spatials(depthFrame, (x,y)) # centroid == x/y in our case
            
            x_depth = spatials['x']
            y_depth = spatials['y']
            z_depth = spatials['z']

            text.rectangle(disp, (x-delta, y-delta), (x+delta, y+delta))
            text.putText(found_rasps, "X: " + ("{:.1f}mm".format(x_depth) if not math.isnan(x_depth) else "--"), (x + 10, y + 20))
            text.putText(found_rasps, "Y: " + ("{:.1f}mm".format(y_depth) if not math.isnan(y_depth) else "--"), (x + 10, y + 35))
            text.putText(found_rasps, "Z: " + ("{:.1f}mm".format(z_depth) if not math.isnan(z_depth) else "--"), (x + 10, y + 50))

            if delay <= 0:
                if (not math.isnan(x_depth)) and (not math.isnan(y_depth)) and (not math.isnan(z_depth)):
                    depth4precision.append((x_depth, y_depth, z_depth))   
                    sum_x += x_depth
                    sum_y += y_depth
                    sum_z += z_depth
                    all_x.append(x_depth)
                    all_y.append(y_depth)
                    all_z.append(z_depth)

                    if len(depth_coord) == 0:
                        depth_coord.append((x_depth, y_depth, z_depth))         
                        num_rasp += 1

                    elif (np.abs(depth_coord[num_rasp-1][0]-x_depth) > 20) and (np.abs(depth_coord[num_rasp-1][1]-y_depth) > 20) and (np.abs(depth_coord[num_rasp-1][2]-z_depth) > 20):
                        depth_coord.append((x_depth, y_depth, z_depth))         
                        num_rasp += 1
        delay = delay - 1
        cameraOn = cameraOn - 1
        cv.imshow("detected circles", found_rasps)
        cv.imshow("depth", disp)
        cv.imshow("reds", new_hue)

        if cv.waitKey(1) == ord('q'):
            break


avg_x = sum_x/len(depth4precision)
avg_y = sum_y/len(depth4precision)
avg_z = sum_z/len(depth4precision)

print('Average x:',avg_x,'\nAverage y:', avg_y,'\nAverage z:', avg_z)
print(len(depth_coord))

samples = list(range(1,len(depth4precision)+1))

plt.figure(figsize=(9, 3))

plt.subplot(131)
plt.plot(samples, all_x, 'b.')
# plt.scatter('average x', avg_x)
plt.subplot(132)
plt.plot(samples, all_y, 'b.')
# plt.scatter('average y', avg_y)
plt.subplot(133)
plt.plot(samples, all_z, 'b.')
# plt.scatter('average z', avg_z)
plt.suptitle('Measurements [mm] (x, y, z)')
plt.show()