# import numpy as np
# from Dynamics import Dynamics

class Controller():

    def __init__(self, Kp_gripper, Kd_gripper, Ki_gripper, previous_error, error_sum):#, robot, Kp, Kd,):
        # self.robot = robot
        # self.Kp = Kp
        # self.Kd = Kd
        self.previous_error = previous_error
        self.error_sum = error_sum
        self.Kp_gripper = Kp_gripper
        self.Kd_gripper = Kd_gripper
        self.Ki_gripper = Ki_gripper


    # def PDCentralized(self, q, q_d, dq, dq_d, ddq_d):
    #     """PD Centralized controller for the manipulator"""
    #     dyn = Dynamics(self.robot)
    #     M = dyn.get_MassDynamics(q)
    #     G = dyn.get_GravityDynamics(q)
    #     Phi = dyn.get_VelocityDynamics(q, dq)
    #     return Phi + G + M @ (self.Kp @ (q_d - q) + self.Kd @ (dq_d - dq) - ddq_d)

    
    def PID_gripper(self, error):#, force2motor):
        """ PID controller for the gripper """
        self.error_sum += error
        error_dif = error - self.previous_error
        self.previous_error = error

        return self.Kp_gripper*error + self.Ki_gripper*self.error_sum - self.Kd_gripper*error_dif

    
        
        
        