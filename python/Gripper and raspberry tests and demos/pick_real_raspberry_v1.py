import numpy as np
from vision import Vision
from harvest import Harvest
from reset_robot import Reset_Robot
# from evaluation import Evaluate_Pick
from time import time, sleep
from PID_Gains import *


def main():
    raspberry = np.array([0.44284730379510195, 0.08563002157041417, 0.19350640407701078, -1.1842252158574382, 1.208264572198466, -1.2360783373773572])

    pull_vec = np.array([0, 0, -0.03]) 
    vis = Vision()
    harv  = Harvest()
    reset = Reset_Robot()
    # eval  = Evaluate_Pick()
    zeroTime = time()
    # Open gripper
    while not harv.is_open():
        harv.move_gripper(10)
    harv.move_gripper(0)
    # Move to raspberry
    reset.move_arm(raspberry, False)
    check_stem = True
    harv.set_controller(K1[0], K1[1], K1[2])
    Fd = Fd1 
    pulling_force = []
    compression_loadcell = []
    # Grasp raspberry
    error = 16  
    timer = time()
    count = 0
    while np.abs(error) > 15:
        error, compression_force, _ = harv.control_gripper(Fd, False)
        compression_loadcell.append(compression_force)
        pulling_force.append(harv.get_gripper_vertical_force())
        t = time() - zeroTime
        # eval.plot_data(compression_force, 0, Fd, 0, 0, harv.get_gripper_vertical_force(), t)
        count += 1
    print((time() - timer )/count)
    harv.move_gripper(0) 
    sleep(0.5) 
    # Pull raspberry
    pos = reset.get_final_pos(pull_vec)
    reset.move_arm(pos, True)
    # Release and hold raspberry
    error = 12
    while True:
        if not check_stem and np.abs(error) < 10:
            break
        error, compression_force, action = harv.control_gripper(Fd, harv.is_off_stem_gripper())
        if harv.is_off_stem_gripper() and check_stem:
            Fd = Fd2
            check_stem = False
            print('OFF STEM ')
            harv.set_controller(K2[0], K2[1], K2[2])
        compression_loadcell.append(compression_force)
        pulling_force.append(harv.get_gripper_vertical_force())
        t = time() - zeroTime
        # eval.plot_data(compression_force, 0, Fd, 0, 0, harv.get_gripper_vertical_force(), t)
    harv.move_gripper(0)

if __name__ == '__main__':

    main()


