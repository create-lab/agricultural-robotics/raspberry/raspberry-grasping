from vision import Vision

vis = Vision()
vis.open_raspi_cam()
# Align x coordinate
aligned = False
while not aligned:
    frame = vis.raspi_cam_frame()
    rasp_coord,_,_ = vis.get_rasperries(frame)
    aligned, direction = vis.align_frame(rasp_coord, [int(frame.shape[1]/2),int(frame.shape[0]/2)], 0)
    print(aligned, direction)
# Align y coordinate
aligned = False
while not aligned:
    frame = vis.raspi_cam_frame()
    rasp_coord,_,_ = vis.get_rasperries(frame)
    aligned, direction = vis.align_frame(rasp_coord, [int(frame.shape[1]/2),int(frame.shape[0]/2)], 1)
    print(aligned, direction)
vis.close_raspi_cam()