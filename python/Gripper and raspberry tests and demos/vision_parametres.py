# Tunable parametres for vision

# RGB window
w_rgb     = 640
h_rgb     = 400
interLeaved = False

# Stereo Parametres
LR_check = False
subPixel = False
extDisparity = True

# Camera window offset
y_init    = 60
x_init    = 50#55
h_adj     = 300
w_adj     = 480
scale     = w_adj/w_rgb

# Blur kernel
n_blur    = 9

# Hue threshold
thresh    = 160#150 

# Circle Hough Transform Parameters
minDist   = 30
param1    = 100
param2    = 17
minRadius = 1
maxRadius = 50

# Define how many frames will be analysed and the initial delay 
delay     = 150
cameraOn  = 400

# Region of interest margin
delta     = 5