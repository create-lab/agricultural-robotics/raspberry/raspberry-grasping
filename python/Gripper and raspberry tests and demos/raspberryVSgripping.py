from time import sleep, time
import numpy as np
import sys

from yaml import load
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
import json
import socket
from Control import Controller

# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871

loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
loadcell.connect_and_handshake()

gripper = dynamixel(ID=2, descriptive_device_name="Gripper", series_name="xl", baudrate=1000000, port_name="COM4")
gripper.begin_communication()
gripper.set_operating_mode("position")

pressureSensor = Arduino(descriptiveDeviceName="Pressure sensor arduino", portName="COM6", baudrate=115200)
pressureSensor.connect_and_handshake()

# Define what data to send to plotjuggler
newData = {
    "zero":0
}

def main():
    ## Number of trials
    n = 5
    trial = 1
    ## Trial duration
    duration = 60 #s

    ## Dynamixel max positions
    open_position = 1400
    close_position = 3800
    velocity = 50

    ## Gripper gains
    Kp_gripper = 1
    Kd_gripper = 0
    Ki_gripper = 0.01

    F_d = 40
    F_rest = 10
    rest = 1
    end_rest = 0

    offset = find_offset(pressureSensor, ["pressure1"])[0]
    print("The baseline threshold is: ", offset)
    s1 = Fluidic_Sensor(offset)

    while trial <= n:
        file = open('F_grip vs raspberry F_d='+str(F_d)+' trial='+str(trial)+'.txt', 'w+')
        file.write('\n\n time    ||    gripping force   ||   raspberry measurement \n\n')

        previous_error = 0
        error_sum = 0

        # offset = find_offset(pressureSensor, ["pressure1"])[0]
        # print("The baseline threshold is: ", offset)
        # s1 = Fluidic_Sensor(offset)

        ctrl = Controller(Kp_gripper, Kd_gripper,Ki_gripper, previous_error, error_sum)

        # while gripper.read_position() > open_position+10:
        #     gripper.write_position(gripper.read_position()-100)
        # sleep(1)

        zeroTime = time()
        while True:
            t = time() - zeroTime
            loadcell.receive_message()
            pressureSensor.receive_message(printOutput=False)

            if loadcell.newMsgReceived:
                compression_force = np.abs(float(loadcell.receivedMessages['lc2']))

                # Go to rest position
                if abs(F_rest-compression_force) > 1 and rest == 1:
                    action = ctrl.PID_gripper(error = F_rest - compression_force) + gripper.read_position()
                    if action >= open_position and action <= close_position:
                        gripper.write_velocity(velocity)
                        gripper.write_position(int(action))
                        print('rest')
                elif abs(F_rest-compression_force) <= 1 and rest == 1:
                    end_rest += 1
                    if end_rest > 100:
                        rest = 0
                        print('end rest')
                else:
                    action = ctrl.PID_gripper(error = F_d - compression_force) + gripper.read_position()
                    if action >= open_position and action <= close_position:
                        gripper.write_velocity(velocity)
                        gripper.write_position(int(action))
                newData["gripping force"] = compression_force
                newData["desired gripping force"] = F_d
                newData["control action"] = int(action)

            if pressureSensor.newMsgReceived:            
                # Get raw measurement
                raw = float(pressureSensor.receivedMessages["pressure1"])
                newData["timestamp"] = t
                newData["raw pressure"] = raw

                # Perform data processing and auto drift compensation
                #s1.auto_drift_compensation(raw, t)
                s1.smooth_signal(raw, t)

                # Obtain processed data
                output, output_compensated = s1.get_processed_signal()

                newData["output"]               = output 
                newData["output compensated"]   = output_compensated

                file.write('      %d' % t+'       %f' % compression_force+'       %f \n' % output)
              
            # Send to plot juggler            
            sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 

            

            if int(t) > duration:
                file.close()
                trial += 1
                rest = 1
                end_rest = 0
                if trial == n+1 and F_d < 200:
                    trial = 1
                    F_d = F_d + 20
                break
        



if __name__ == '__main__':
    main()
