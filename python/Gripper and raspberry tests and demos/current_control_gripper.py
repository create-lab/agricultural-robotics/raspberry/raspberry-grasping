from time import sleep, time
import numpy as np
import sys

from yaml import load

from harvest import Harvest
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
import json
import socket


sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871


# Define what data to send to plotjuggler
newData = {
    "zero":0
}

def main():

    file = open('ForceVsCurrent.txt', 'w+')
    file.write('current          |          loadcell\n')
    harv = Harvest()
    zeroTime = time()
    action = 0
    counter = 0
    harv.grasp_raspberry()
    sleep(0.5)
    compression_force = 0
    while compression_force <= 450 and action<100:
        while counter < 100:
            t = time() - zeroTime
            harv.move_gripper(-action)
            compression_force = harv.get_compression_force()
            
                
            print('control action: '+str(-action)+'     lc2: '+str(compression_force))
            file.write('      %i' % -action+'       %f\n' %compression_force)
                
            newData["gripping force"] = compression_force
            newData["control action"] = int(action)
            newData["timestamp"] = t
                
                # action = action - 0.1
            # Send to plot juggler            
            sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 
            counter += 1
        action += 1
        counter = 0
    file.close()


if __name__ == '__main__':
    main()
