from typing import final
import numpy as np
import cv2 as cv
from Vision import *
import matplotlib.pyplot as plt
import depthai as dai
from gen2_calc_spatials_on_host.calc import HostSpatialsCalc
from gen2_calc_spatials_on_host.utility import *
import math
from scipy.cluster.vq import vq, kmeans, whiten
import rtde_receive
import rtde_control
from time import sleep

## GOOD PLACEMENT OF THE CAMERA: about 545mm away from raspberry and 200mm from the table ##

## UR3 set up
# rtde_c = rtde_control.RTDEControlInterface("192.168.1.22")
# rtde_r = rtde_receive.RTDEReceiveInterface("192.168.1.22")
start_pos = [0, -90.00 , -90.00, 0, 90.00, 0]
# Speeds and accelerations - Joint space
speed_J_fast = 0.4
acc_J_fast = 0.3
# Speeds and accelerations - Cartesian space
speed_L_fast = 0.4
acc_L_fast = 0.3
# Parameters for camera to UR3 coordinate transformation
p_cam = np.array([[0.04959681137681763], [0.2682097722067104],[0.2]]) #[m] camera position on the UR3 referencial
phi = 20*math.pi/180 # angle between camera referencial and UR3 referencial
R = np.array([[np.sin(phi), np.cos(phi), 0], [0, 0, 1], [np.cos(phi), -np.sin(phi), 0]]) # Rotation matrix
z_offset = np.array([[0],[0],[0.17]]) # camera z measurement systematic error
robot_offset = np.array([[0],[0.06],[0.03]]) # systematic error after transformation
## Tunable parametres
# RGB window
w_rgb     = 640
h_rgb     = 400
# Camera window offset
y_init    = 60
x_init    = 50#55
h_adj     = 300
w_adj     = 480
scale     = w_adj/w_rgb
# Blur kernel
n_blur    = 9
# Hue threshold
thresh    = 160#150 
# CHT Parameters
minDist   = 30
param1    = 100
param2    = 17
minRadius = 1
maxRadius = 50
# Define how many frames will be analysed and the initial delay 
delay     = 150
cameraOn  = 400

# Create pipeline
pipeline = dai.Pipeline()

# Define source and output
camRgb = pipeline.create(dai.node.ColorCamera)
monoLeft = pipeline.create(dai.node.MonoCamera)
monoRight = pipeline.create(dai.node.MonoCamera)
stereo = pipeline.create(dai.node.StereoDepth)

# Properties
camRgb.setPreviewSize(w_rgb, h_rgb)
camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
camRgb.setInterleaved(False)
camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

stereo.initialConfig.setConfidenceThreshold(255)
stereo.setLeftRightCheck(False)
stereo.setSubpixel(False)
stereo.setExtendedDisparity(True)

# Linking
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)

xoutRgb = pipeline.create(dai.node.XLinkOut)
xoutRgb.setStreamName("rgb")
camRgb.preview.link(xoutRgb.input)

xoutDepth = pipeline.create(dai.node.XLinkOut)
xoutDepth.setStreamName("depth")
stereo.depth.link(xoutDepth.input)

xoutDepth = pipeline.create(dai.node.XLinkOut)
xoutDepth.setStreamName("disp")
stereo.disparity.link(xoutDepth.input)


# Connect to device and start pipeline
with dai.Device(pipeline) as device:

   # Output queue will be used to get the rgb frames from the output defined above
    qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
    depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
    dispQ = device.getOutputQueue(name="disp")

    hostSpatials = HostSpatialsCalc(device)
    text = TextHelper()
    delta = 5
    hostSpatials.setDeltaRoi(delta)

    all_depth_coord = []
    depth_coord = []

    sum_rasp = 0

    # to measure precision
    sum_x = 0
    sum_y = 0
    sum_z = 0
    all_x = []
    all_y = []
    all_z = []

    while cameraOn > 0:
        image  = qRgb.get().getCvFrame() # blocking call, will wait until a new data has arrived

        rasp_coords, _, found_rasps = getRasperries(image, scale, n_blur, thresh, minDist, param1, param2, minRadius, maxRadius)

        depthFrame = depthQueue.get().getFrame() 
        depthFrame = depthFrame[y_init:y_init+h_adj, x_init:x_init+w_adj]

        # Get disparity frame for nicer depth visualization
        disp = dispQ.get().getFrame()
        disp = (disp * (255 / stereo.initialConfig.getMaxDisparity())).astype(np.uint8)
        disp = cv.applyColorMap(disp, cv.COLORMAP_JET)
        disp = disp[y_init:y_init+h_adj, x_init:x_init+w_adj]

        for raspberry in rasp_coords:
           
            # Calculate spatial coordiantes from depth frame
            (x, y) = (raspberry[0],raspberry[1])
            spatials, centroid = hostSpatials.calc_spatials(depthFrame, (x,y)) # centroid == x/y in our case
            
            x_depth = spatials['x']
            y_depth = spatials['y']
            z_depth = spatials['z']

            text.rectangle(disp, (x-delta, y-delta), (x+delta, y+delta))
            text.putText(found_rasps, "X: " + ("{:.1f}mm".format(x_depth) if not math.isnan(x_depth) else "--"), (x + 10, y + 20))
            text.putText(found_rasps, "Y: " + ("{:.1f}mm".format(y_depth) if not math.isnan(y_depth) else "--"), (x + 10, y + 35))
            text.putText(found_rasps, "Z: " + ("{:.1f}mm".format(z_depth) if not math.isnan(z_depth) else "--"), (x + 10, y + 50))

            if delay <= 0:
                if (not math.isnan(x_depth)) and (not math.isnan(y_depth)) and (not math.isnan(z_depth)):
                    all_depth_coord.append((x_depth, y_depth, z_depth))   
                    sum_rasp += len(rasp_coords)
                    all_x.append(x_depth)
                    all_y.append(y_depth)
                    all_z.append(z_depth)


        delay = delay - 1
        cameraOn = cameraOn - 1
        cv.imshow("detected circles", found_rasps)
        cv.imshow("depth", disp)
        # cv.imshow("reds", new_hue)

        if cv.waitKey(1) == ord('q'):
            break

total_rasp = np.ceil(sum_rasp/len(all_depth_coord))
features = np.array(all_depth_coord)
whitened = whiten(features)
clusters = kmeans(whitened,int(total_rasp))

centroids = clusters[0]

x_std = np.std(np.array(all_x))
y_std = np.std(np.array(all_y))
z_std = np.std(np.array(all_z))

final_coords = centroids*np.array([x_std, y_std, z_std])
final_coords = final_coords.tolist()

# # Find outliers
# # points array will be used to reach the index easily
# points = np.empty((0,len(all_coord[0])), float)

# # distances will be used to calculate outliers
# distances = np.empty((0,len(all_coord[0])), float)

# # getting points and distances
# for i, center_elem in enumerate(denormalized):
#     # cdist is used to calculate the distance between center and other points
#     distances = np.append(distances, cdist([center_elem],all_coord[labels == i], 'euclidean')) 
#     points = np.append(points, all_coord[labels == i], axis=0)

# # Remove outliers
# outliers = points[np.where(distances > np.percentile(distances, percentile))]
# new_data = points[np.where(distances <= np.percentile(distances, percentile))]

# # Recalculate clusters
# whitened = whiten(new_data)
# (centroids, labels) = kmeans2(whitened,int(total_rasp))

# final_coords = []
# for i, center_elem in enumerate(centroids):
#     final_coords.append(np.mean(new_data[labels == i], axis=0))

print('Total number of raspberries:', total_rasp)
print(final_coords)

for coord in final_coords:
    coord = np.array([[-coord[0]/1000],[coord[1]/1000],[coord[2]/1000]])
    coordUR3 = (R.T@(coord-z_offset))+p_cam-robot_offset
    print(coord)
    print(coordUR3)
#     # Get current position in Cartesian space
#     rtde_c.moveJ(np.deg2rad(start_pos), speed_J_fast, acc_J_fast)
#     sleep(1)
#     temp_L = rtde_r.getActualTCPPose()
#     temp_L[0] = coordUR3[0][0]
#     temp_L[1] = coordUR3[1][0]
#     temp_L[2] = coordUR3[2][0]
#     print(temp_L)
#     rtde_c.moveL(temp_L, speed_L_fast, acc_L_fast)
#     # pick the raspbery
#     sleep(2) 
#     rtde_c.moveJ(np.deg2rad(start_pos), speed_J_fast, acc_J_fast)

samples = list(range(1,len(all_depth_coord)+1))

plt.figure(figsize=(9, 3))

plt.subplot(131)
plt.plot(samples, all_x, 'b.')
# plt.scatter('average x', avg_x)
plt.subplot(132)
plt.plot(samples, all_y, 'b.')
# plt.scatter('average y', avg_y)
plt.subplot(133)
plt.plot(samples, all_z, 'b.')
# plt.scatter('average z', avg_z)
plt.suptitle('Measurements [mm] (x, y, z)')
plt.show()



