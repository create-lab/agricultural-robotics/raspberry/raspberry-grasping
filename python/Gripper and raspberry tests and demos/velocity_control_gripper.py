from time import sleep, time
import numpy as np
import sys

from yaml import load
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
import json
import socket
from Control import Controller


sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871

gripper = dynamixel(ID=112, descriptive_device_name="Gripper", series_name="xm", baudrate=1000000, port_name="COM4")
gripper.begin_communication()
gripper.set_operating_mode("velocity")

loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
loadcell.connect_and_handshake()


# Define what data to send to plotjuggler
newData = {
    "zero":0
}

def main():

    ## Dynamixel max positions
    open_position = 900
    close_position = -300

    ## Gripper gains
    Kp_gripper = 0.05
    Ki_gripper = 0.0
    Kd_gripper = 0.0

    
    previous_error = 0
    error_sum = 0

    F_d = 350


    ctrl = Controller(Kp_gripper, Kd_gripper,Ki_gripper, previous_error, error_sum)

    zeroTime = time()
    while True:
        t = time() - zeroTime
        loadcell.receive_message()

        if loadcell.newMsgReceived:
            compression_force = np.abs(float(loadcell.receivedMessages['lc2']))

            action = ctrl.PID_gripper(error = compression_force - F_d)
            if action > 330:
                action = 330

            if action < 0 and gripper.read_position() <= close_position:
                print('closed')
                action = 0

            if gripper.read_position() >= open_position and action > 0:
                print('open')
                action = 0

            gripper.write_velocity(action)
            
            newData["gripping force"] = compression_force
            newData["desired gripping force"] = F_d
            newData["control action"] = action
            
        # Send to plot juggler            
        sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 


if __name__ == '__main__':
    main()
