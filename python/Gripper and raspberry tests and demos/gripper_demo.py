from time import sleep
import numpy as np
import sys
sys.path.append('libraries/')


import rtde_receive
import rtde_control
from libraries.dynamixel_controller import dynamixel
from libraries.keyboard import Key

key = Key()

ur_read = rtde_receive.RTDEReceiveInterface("192.168.1.22")
ur_write = rtde_control.RTDEControlInterface("192.168.1.22")

home_pos = [0, -1.57085592, 0, -1.57077247e+00, 0, 0]

open_position = 3333
close_position = 1650

gripper = dynamixel(ID=1, descriptive_device_name="Gripper", 
                    series_name="xm", baudrate=1000000, port_name="/dev/ttyUSB0")

gripper.begin_communication()
gripper.set_operating_mode("position")


def move_home(): 
    ur_write.moveJ(home_pos, 0.4, 0.3)

def move_xyz_relative(x=0,y=0,z=0, vel = 0.1, acc = 0.1):
    target_pose = np.copy(ur_read.getActualTCPPose())
    target_pose[0] += x
    target_pose[1] += y
    target_pose[2] += z

    ur_write.moveL(target_pose, vel, acc)

def move_to_raspberry_nearby(): 
    move_home()
    print("Now at home")

    targetJ = np.copy(ur_read.getActualQ())
    targetJ[2] -= np.pi/2
    targetJ[3] -= np.pi/2
    targetJ[4] -= np.pi/2

    ur_write.moveJ(targetJ, 0.5, 0.4)

    move_xyz_relative(x=-0.05, y=0.2, z=-0.05)

def harvest_raspberry():
    move_xyz_relative(x=0.1, vel = 0.01)
    gripper.write_position(close_position)

    sleep(1)

    move_xyz_relative(z = -0.03, vel = 0.01)
    sleep(1.5)

    gripper.write_position(open_position)
    sleep(1)

    move_xyz_relative(x = -0.1, vel = 0.01)
    move_xyz_relative(z = 0.03, vel = 0.01)

def main(): 
    gripper.write_position(open_position)
    move_to_raspberry_nearby()

    while 1: 
        val = input("enter q to escape")
        
        if val == "q":
            break
        else:
            harvest_raspberry()

    move_home()

if __name__ == "__main__": 
    main()