import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

dt = 0.01
tfinal = 1
x0 = 0

sqrtdt = np.sqrt(dt)
n = int(tfinal/dt)
xtraj = np.zeros(n+1, float)
trange = np.linspace(start=0,stop=tfinal ,num=n+1)
xtraj[0] = x0

for i in range(n):
    xtraj[i+1] = xtraj[i] + np.random.normal()

x = trange
y = xtraj

# animation line plot example

fig, ax = plt.subplots(1, 1, figsize = (6, 6))

def animate(i):
    x_plot = x[:i]
    y_plot = y[:i]

    ax.cla() # clear the previous image
    ax.plot(x_plot, y_plot) # plot the line

    ax.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off


    try:
        if x_plot[-1] < 0.4:
            x_upper = 0.4
            x_lower = 0
        else:
            x_upper = x_plot[-1]
            x_lower = x_upper - 0.4
        ax.set_xlim([x_lower, x_upper]) # fix the x axis

    except:
        ax.set_xlim([x0, tfinal]) # fix the x axis

    ax.set_ylim([1.1*np.min(y), 1.1*np.max(y)]) # fix the y axis


anim = animation.FuncAnimation(fig, animate, frames = len(x) + 1, interval = 1, blit = False)
plt.show()