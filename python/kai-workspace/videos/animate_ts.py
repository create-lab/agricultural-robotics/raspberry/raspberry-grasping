import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from utility import *

data = {}

target_names = {"Compression":"Fc", "Pulling":"Fp", 
                    "Time":"Time", "Init":"Arm_pos", 
                    "Gripper":"Gripper_pos", "Twin_fc":"Twin_fc", 
                    "Twin_fp":"Twin_fp"}

color_pallet = {"Twin_fp":"#E14D2A", "Twin_fc":"#FD841F", "Fp":"#3E6D9C", "Fc":"#001253"}
target_ylabel = {"Twin_fp":"$F_{p,T}$ (N)", "Twin_fc":"$F_{c,T}$ (Arb. unit)", "Fp":"$F_{p,R}$ (N)", "Fc":"$F_{c,R}$ (N)"}

multiplier = {"Twin_fp":(9.8 / 1000), "Twin_fc":1, "Fp":(9.8 / 1000), "Fc":(9.8 / 1000)}

target_keys = list(target_names.keys())

save_path = '/home/kaijunge/Documents/PhD/CREATE Lab Git Repo/Raspberry/raspberry-grasping/python/kai-workspace/videos/ts_animation/harv_only'
base_path = '/home/kaijunge/Documents/PhD/CREATE Lab Git Repo/Raspberry/raspberry-grasping/python/kai-workspace/videos/getting_videos_only/multiple harvests/1/Harvesting data day 2'

folder_path = base_path
for subdir, dirs, files in os.walk(folder_path):
    if subdir == folder_path:
        continue

    folder_name = subdir[len(folder_path) + 1:]

    # print(folder_name)
    
    target = ""
                
    for key in target_keys:
        if key in folder_name:
            target = target_names[key]
            break

    # print(target)

    if target != "":
        for file in files:
            f = subdir + "/" + file
            csv = np.genfromtxt(f, delimiter=',')

            data[target] = csv


data_keys = list(data.keys())

for dk in data_keys:
    print(dk, len(data[dk]))

time_vector = data["Time"] - data["Time"][0]

t_window = 1

# "Twin_fp" "Twin_fc" "Fc" "Fp"
target_quantity = "Fp"

x_raw = time_vector
y_raw = data[target_quantity] * multiplier[target_quantity]

y_label = target_ylabel[target_quantity]

# Resample 30fps
x, y = resample(x_raw, y_raw)

fig, ax = plt.subplots(1, 1, figsize = (6, 4))

def animate(i):
    x_plot = x[:i]
    y_plot = y[:i]

    ax.cla() # clear the previous image
    ax.plot(x_plot, y_plot, linewidth = 4, color = color_pallet[target_quantity]) # plot the line

    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False) 
    
    try:
        if x_plot[-1] >= t_window:
            x_upper = x_plot[-1]
            x_lower = x_upper - t_window
        ax.set_xlim([x_lower, x_upper]) # fix the x axis

    except:
        ax.set_xlim([0, t_window]) # fix the x axis

    ax.set_ylim([1.1*np.min(y), 1.1*np.max(y)]) # fix the y axis

    ax.set_ylabel(y_label, fontsize = 30)
    ax.tick_params(axis='y', which='major', labelsize=20)
    ax.grid(axis = 'y')



anim = animation.FuncAnimation(fig, animate, frames = len(x) + 1, interval = 33, blit = False)
# plt.tight_layout()
plt.subplots_adjust(left = 0.2)
plt.show()

f = save_path + '/' + '1' + target_quantity + '.mp4'# "c://Users/xx/Desktop/animation.mp4" 
writervideo = animation.FFMpegWriter(fps=30) 
anim.save(f, writer=writervideo, savefig_kwargs={"transparent": True})