import numpy as np


def resample(x_raw, y_raw, n_smooth = 5):
    x = []
    y = []

    for _ in range(n_smooth):
        y.append(0)

    resample_time = 0
    while resample_time <= x_raw[-1]:
        for i in range(len(x_raw)):
            if x_raw[i] > resample_time:
                low_bound = x_raw[i-1]
                up_bound = x_raw[i]

                index = i
                break

        # get y value
        ratio = (up_bound - resample_time) / (up_bound - low_bound)
        print(ratio)
        y_value = y_raw[index - 1] + (y_raw[index] - y_raw[index - 1]) * ratio


        x.append(resample_time)
        y.append(y_value)

        resample_time += 1/30
        
    y_final = y[-1]

    for _ in range(n_smooth):
        y.append(y_final)

    window = n_smooth
    w = np.ones(window)/window
    y = np.convolve(y, w, mode='same')

    y = y[5:-5]

    return x, y

