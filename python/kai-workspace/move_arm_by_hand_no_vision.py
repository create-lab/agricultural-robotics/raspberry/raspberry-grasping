import numpy as np
from reset_robot import Reset_Robot
from libraries.keyboard import Key


def main():
    # Position the arm takes to detect the raspberries
    # NOTE: MIGHT NEED TO BE CHANGED ON THE FIELD 
    
    # original starting joitns 
    start_joints = [np.pi/4, -0.8411205450641077, -2.3005085627185267, 0.006565093994140625, 1.5466278791427612, 3.5952674807049334e-05]
    
    # new starting joints
    '''
    UR3
    '''
    # start_joints = [-0.011756722127095998, -1.1548779646502894, -1.640467945729391, -0.3213289419757288, 1.5581222772598267, -3.5587941304981996e-05]
    
    # start_joints = [-0.009755436574117482, -1.0037253538714808, -2.045485798512594, -0.06473380724062139, 1.556533694267273, 0.00022732577053830028]

    # start_joints = [0.16518868505954742, -1.0390570799456995, -2.0151713530169886, -0.045069519673482716, 1.381521224975586, -0.018224541340963185]

    # start_joints = [0.4097597599029541, -1.061399284993307, -1.894759480153219, -0.11638051668276006, 1.1427284479141235, -0.03593618074526006]

    '''
    UR5
    '''
    start_joints = [0.7780196666717529, -0.9984992186175745, -2.245772663746969, 0.10938823223114014, 1.5539401769638062, 0.00027563716867007315]

    reset = Reset_Robot()
    key = Key()
    input()
    reset.move_arm_joints(start_joints)
    # input()
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Align raspberry with OAK-D RGB camera
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    actual_direction = ""
    while True:

        demand_direction = key.keyPress

        if demand_direction is not actual_direction:
            actual_direction = demand_direction
            if demand_direction == "a":
                arm_speed = [-0.01, 0, 0]
            elif demand_direction == "d":
                arm_speed = [0.01, 0, 0] 
            elif demand_direction == "w":
                arm_speed = [0, -0.01, 0]
            elif demand_direction == "s":
                arm_speed = [0, 0.01, 0]
            elif demand_direction == "z":
                arm_speed = [0, 0, 0.01]
            elif demand_direction == "x":
                arm_speed = [0, 0, -0.01]
            elif demand_direction == "p":
                print(reset.get_joints())
            else:
                reset.stop_arm()
                continue
            
            R = reset.get_R06('UR5')
            arm_speed = R@arm_speed
        
            pos = reset.change_arm(arm_speed, 0)
            reset.move_arm(pos)

        # reset.move_arm_joints(start_joints)

if __name__ == '__main__':
    main()