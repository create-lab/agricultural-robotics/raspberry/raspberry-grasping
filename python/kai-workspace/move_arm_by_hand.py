import numpy as np
import depthai as dai
from vision import Vision
from reset_robot import Reset_Robot
from libraries.keyboard import Key

import cv2 as cv


def main():
    # Position the arm takes to detect the raspberries
    # NOTE: MIGHT NEED TO BE CHANGED ON THE FIELD 
    
    # original starting joitns 
    start_joints = [0, -0.8411205450641077, -2.3005085627185267, 0.006565093994140625, 1.5466278791427612, 3.5952674807049334e-05]
    
    # new starting joints
    '''
    UR3
    '''
    # start_joints = [-0.011756722127095998, -1.1548779646502894, -1.640467945729391, -0.3213289419757288, 1.5581222772598267, -3.5587941304981996e-05]
    
    # start_joints = [-0.009755436574117482, -1.0037253538714808, -2.045485798512594, -0.06473380724062139, 1.556533694267273, 0.00022732577053830028]

    # start_joints = [0.16518868505954742, -1.0390570799456995, -2.0151713530169886, -0.045069519673482716, 1.381521224975586, -0.018224541340963185]

    # start_joints = [0.4097597599029541, -1.061399284993307, -1.894759480153219, -0.11638051668276006, 1.1427284479141235, -0.03593618074526006]

    '''
    UR5
    '''
    start_joints = [0.5187795162200928, -1.0295751730548304, -2.3532496134387415, 0.25610923767089844, 1.8091161251068115, 0.0030072915833443403]

    vis   = Vision(1)
    reset = Reset_Robot()
    key = Key()
    input()
    reset.move_arm_joints(start_joints)
    # input()
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Align raspberry with OAK-D RGB camera
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    pipeline, _ = vis.set_camera_pipeline()
    with dai.Device(pipeline) as device:
        # Output queue will be used to get the rgb frames from the output defined above
        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        
        # Skip the first frames while the camera focus
        count = 0
        while count < 20:
            frame  = qRgb.get().getCvFrame() 
            count += 1

        actual_direction = ""
        demand_direction = ""
        while True:
            # Show camera and detected raspberry
            frame  = qRgb.get().getCvFrame()          	
            flipVertical = cv.flip(frame, 0)
            flipHorizontal = cv.flip(flipVertical, 1)
            centre = [int(frame.shape[1]/2),int(frame.shape[0]/2)]
            cv.circle(flipHorizontal, centre, 1, (255, 255, 0), 3)
            cv.imshow('frame',flipHorizontal)
            if cv.waitKey(1) == ord('q'):
                break 

            demand_direction = key.keyPress

            if demand_direction is not actual_direction:
                actual_direction = demand_direction
                if demand_direction == "a":
                    arm_speed = [-0.01, 0, 0]
                elif demand_direction == "d":
                    arm_speed = [0.01, 0, 0] 
                elif demand_direction == "w":
                    arm_speed = [0, -0.01, 0]
                elif demand_direction == "s":
                    arm_speed = [0, 0.01, 0]
                elif demand_direction == "z":
                    arm_speed = [0, 0, 0.01]
                elif demand_direction == "x":
                    arm_speed = [0, 0, -0.01]
                elif demand_direction == "p":
                    print(reset.get_joints())
                else:
                    reset.stop_arm()
                    continue
                
                R = reset.get_R06('UR5')
                arm_speed = R@arm_speed
            
                pos = reset.change_arm(arm_speed, 0)
                reset.move_arm(pos)

        # reset.move_arm_joints(start_joints)

if __name__ == '__main__':
    main()