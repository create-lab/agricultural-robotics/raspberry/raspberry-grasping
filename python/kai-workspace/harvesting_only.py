import os
import json
import socket
from threading import Thread
from time import sleep, time
import matplotlib.pyplot as plt

import numpy as np

from harvest import Harvest
from PID_Gains import *
from reset_robot import Reset_Robot

from libraries.keyboard import Key


# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *

# Online libraries
from sklearn.linear_model import LinearRegression
from evaluation import Evaluate_Pick


key = Key()

print('\n\n\n')

harv  = Harvest(id)
reset = Reset_Robot()
eval = Evaluate_Pick()

def main(id):


    input("Press enter to move arm to initial position")
    
    # Position the arm takes to detect the raspberries
    # NOTE: MIGHT NEED TO BE CHANGED ON THE FIELD 
    
    harv.move_fingers_to_position(harv.open)

    # start_joints = [0.4539504647254944, -1.3301237265216272, 1.3692049980163574, -3.1960156599627894, -0.32082921663393194, -0.010236088429586232]# basket_joints = [1.81415855884552, -1.359485928212301, -2.449453655873434, -0.4877827803241175, 1.3663604259490967, 0.06709630787372589]
    start_joints = [0.4097597599029541, -1.061399284993307, -1.894759480153219, -0.11638051668276006, 1.1427284479141235, -0.03593618074526006]

    
    pulling_force_init = 0
    time_stamps = []
    # lowest_z = 0.11720719872012049

    zeroTime = time()
    reset.move_arm_joints(start_joints,0.1)

    input()

    move_forward = [0, 0, 0.075] # 32
    R = reset.get_R06('UR3')
    pos = reset.change_arm(R@move_forward, 1)
    reset.move_arm_to_position(pos, 0.06)


    input('\n\nPress Enter to grasp and pull raspberry') 
    time_stamps.append(time() - zeroTime)

    eval.take_offset()
    for _ in range(100):
        fc, fp = eval.get_data2()

    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Grasp and pull        
    #----------------------------------------------------------------------------------------------------------------------------------------------------------s     
    #for debugging #
    # joints raspberry = [0.7063584923744202, -1.6583831946002405, -1.6437566916095179, 0.16852736473083496, 1.6586774587631226, 0.00222756783477962]
    raspberry = reset.get_arm_pose() # keep position of the raspberry
    pick_joints = reset.get_joints()
    init_final_pos = raspberry
    # raspberry = [0.4544665714974, 0.24128676737548374, 0.5356093246361839, -1.5143836568366442, 0.601165144548914, -0.5797220039919777]
    harv.move_fingers_to_position(harv.open)
    sleep(0.5)
    # reset.move_arm_to_position(raspberry, 0.05, 0.1,False)

    check_stem = True
    # detect_slip = False
    fail = False
    # slip_thresh = 40
    # slip_ref = 0
    compression_force = 0
    time_vec = []
    pulling_gripper = []
    compression_loadcell = []
    gripper_positions = []
    twin_fc = []
    twin_fp = []

    F_d1 = harv.set_grasping_force(pulling_force_init)
    Fd = F_d1
    print('Force', Fd)
    pull_speed = [0, 0.01, 0] # pulling speed
    harv.set_controller(K1[0], K1[1], K1[2]) 
    error_margin = 9
    error = error_margin + 1

    while True:
        while np.abs(error) > error_margin:
            # if detect_slip:
                # print('Slip', msg)

            # Check when raspberry gets picked
            if harv.is_off_stem_gripper() and check_stem:
                reset.stop_arm()
                Fd = F_d2
                error_margin = 5
                check_stem = False 
                detect_slip = False
                print('OFF STEM')
                harv.set_controller(K2[0], K2[1], K2[2])   
                harv.change_grasping_force(is_off_stem=True)
                # vis.close_raspi_cam()0


            # NOTE TRY TO RUN THE CONTROLLER IN PARALLEL AS WELL (when the rest is working...)
            error, compression_force, action = harv.control_gripper(Fd, harv.is_off_stem_gripper(),compression_loadcell)
            try: 
                fc, fp = eval.get_data2()
            except:
                fc = 0
                fp = 0
            
            t = time() - zeroTime
            time_vec.append(t)
            compression_loadcell.append(compression_force)
            pulling_gripper.append(harv.get_gripper_vertical_force())
            gripper_positions.append(harv.gripper.read_position())
            twin_fc.append(fc)
            twin_fp.append(fp)

            print(compression_force)

            # If the picking is not detected
            if np.abs(reset.get_arm_pose()[2]-raspberry[2]) >= 0.05 and Fd == F_d1:
            # if abs(reset.get_arm_pose()[2]-lowest_z) >= 0.01 and Fd == F_d1:
                harv.move_gripper(0)
                reset.stop_arm()
                print('\n\n\nOff stem was not detected!')
                fail = True
                break

        if fail:
            print('\n\n\n Fail \n\n\n')
            break
        
        # Pull raspberry
        if np.abs(error) <=  error_margin and Fd == F_d1:
            sleep(0.5)
            error_margin = 0
            R = reset.get_R06('UR3')
            pos = reset.change_arm(R@pull_speed, 0)
            reset.move_arm(pos)
            check_stem = True
            # start getting slip readings

        # Stop if raspberry was picked
        elif np.abs(error) <=  error_margin and Fd == F_d2:
            count = 0 
            while count < 30:
                error, compression_force, action = harv.control_gripper(Fd, harv.is_off_stem_gripper(),compression_loadcell)
                try: 
                    fc, fp = eval.get_data2()
                except:
                    fc = 0
                    fp = 0

                t = time() - zeroTime
                time_vec.append(t)
                compression_loadcell.append(compression_force)
                pulling_gripper.append(harv.get_gripper_vertical_force())
                gripper_positions.append(harv.gripper.read_position())
                twin_fc.append(fc)
                twin_fp.append(fp)

                print(compression_force)

                count += 1
            break
    
    harv.experimentID = id

    harv.move_gripper(0)
    init_final_pos.append(reset.get_arm_pose())
    harv.save_csv_data(init_final_pos, 'Initial_and_final_robot_position'+str(id))
    harv.save_csv_data(compression_loadcell, 'Compression_force_gripper'+str(id))
    harv.save_csv_data(pulling_gripper, 'Pulling_force_gripper'+str(id))
    harv.save_csv_data(time_vec, 'Time_vector'+str(id))
    harv.save_csv_data(gripper_positions, 'Gripper positions'+str(id))
    harv.save_csv_data(twin_fc, 'Twin_fc'+str(id))
    harv.save_csv_data(twin_fp, 'Twin_fp'+str(id))

    
    harv.move_fingers_to_position(harv.hold_pos)
    # input("Press enter for graph")
    # plt.plot(np.linspace(1, 10, len(twin_fc)), twin_fc)
    # plt.show()


    input("press to move up")
    reset.move_arm_joints(pick_joints,0.1)

    input("press to wiggle")

    side = 0.03
    up = 0.014

    move_offset = [side, 0, 0] # 32
    R = reset.get_R06('UR3')
    pos = reset.change_arm(R@move_offset, 1)
    reset.move_arm_to_position(pos, 0.05)

    for _ in range(2):

        move_up = [0, -up, 0] #[0, -0.045, 0]
        R = reset.get_R06('UR3')
        ur_pose = reset.change_arm(R@move_up, 1)
        reset.move_arm_to_position(ur_pose, 0.05)
        move_up = [0, up, 0] #[0, -0.045, 0]
        R = reset.get_R06('UR3')
        ur_pose = reset.change_arm(R@move_up, 1)
        reset.move_arm_to_position(ur_pose, 0.05)

        move_offset = [-side * 2, 0, 0] # 32
        R = reset.get_R06('UR3')
        pos = reset.change_arm(R@move_offset, 1)
        reset.move_arm_to_position(pos, 0.05)

        move_up = [0, -up, 0] #[0, -0.045, 0]
        R = reset.get_R06('UR3')
        ur_pose = reset.change_arm(R@move_up, 1)
        reset.move_arm_to_position(ur_pose, 0.05)
        move_up = [0, up, 0] #[0, -0.045, 0]
        R = reset.get_R06('UR3')
        ur_pose = reset.change_arm(R@move_up, 1)
        reset.move_arm_to_position(ur_pose, 0.05)

        move_offset = [side * 2, 0, 0] # 32
        R = reset.get_R06('UR3')
        pos = reset.change_arm(R@move_offset, 1)
        reset.move_arm_to_position(pos, 0.05)

    move_up = [0, -up, 0] #[0, -0.045, 0]
    R = reset.get_R06('UR3')
    ur_pose = reset.change_arm(R@move_up, 1)
    reset.move_arm_to_position(ur_pose, 0.05)
    move_up = [0, up, 0] #[0, -0.045, 0]
    R = reset.get_R06('UR3')
    ur_pose = reset.change_arm(R@move_up, 1)
    reset.move_arm_to_position(ur_pose, 0.05)    

    move_offset = [-side, 0, 0] # 32
    R = reset.get_R06('UR3')
    pos = reset.change_arm(R@move_offset, 1)
    reset.move_arm_to_position(pos, 0.05)

    input("press to release")

    harv.move_fingers_to_position(harv.open)
    sleep(0.5)

    move_offset = [0, 0, -0.07] # 32
    R = reset.get_R06('UR3')
    pos = reset.change_arm(R@move_offset, 1)
    reset.move_arm_to_position(pos, 0.1)

    input("press to go back")

    reset.move_arm_joints(start_joints,0.1)


if __name__ == '__main__':
    main("harv")