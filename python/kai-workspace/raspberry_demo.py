import cv2 as cv
import numpy as np
import depthai as dai
from time import sleep
from vision import Vision
import rtde_control, rtde_receive

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *


vis   = Vision('demo20220929')

# Dynamixel
gripper = dynamixel(ID=112, descriptive_device_name="Gripper", series_name="xm", baudrate=1000000, port_name="COM4")
gripper.begin_communication()
gripper.set_operating_mode("velocity")

open_pos = 4900
close_pos = 3925
approaching_speed = 30

# Setup robot with robot IP address
rtde_c = rtde_control.RTDEControlInterface("192.168.1.22")
rtde_r = rtde_receive.RTDEReceiveInterface("192.168.1.22")

# Speeds and accelerations - Joint space
speed_J = 0.1
acc = 1

# Speeds and accelerations - Joint space
speed_L = 0.01
acc = 1

# Joint Poses
initial = [-0.07011062303651983, -1.2717660109149378, 1.844879150390625, -3.716628376637594, 0.1339649111032486, 0.01708800718188286]
mid_pos1 = [-0.06981116930116826, -1.3747881094561976, 1.664426326751709, -3.4312074820147913, 0.1324908584356308, 0.015043573454022408]
mid_pos2 = [-0.12475902238954717, -1.69460374513735, 1.9933195114135742, -3.4457533995257776, 0.1873883455991745, 0.017853500321507454]
mid_pos3 = [0.06639935076236725, -1.6475217978106897, 1.9625797271728516, -3.4877308050738733, -0.04356557527651006, 0.011862510815262794]
mid_pos4 = [0.06564396619796753, -1.6901910940753382, 1.6985301971435547, -3.196648422871725, -0.044463459645406544, 0.02783823199570179]
grasp_raspberry = [0.4751591682434082, -1.4185431639300745, 1.4350314140319824, -3.1623199621783655, -0.45091182390321904, -0.011623207722799123]
pull_raspberry = [0.475063294172287, -1.4147284666644495, 1.5030527114868164, -3.2335713545428675, -0.4505050818072718, -0.012197319661275685]

def is_in_position(position):
    if abs(gripper.read_position() - position) <= 5:
        return True
    else:
        return False

def move_fingers_to_position(position):
    current_pos = gripper.read_position()
    if current_pos < position:
        signal = 1
    elif current_pos > position:
        signal = -1
    while not is_in_position(position):
        gripper.write_velocity(approaching_speed*signal)
    gripper.write_velocity(0)

def main():
    # open gripper
    move_fingers_to_position(open_pos)
    rtde_c.moveJ(initial, speed_J, acc)
    sleep(1)
    rtde_c.moveJ(mid_pos1, speed_J, acc)
    sleep(1)
    rtde_c.moveJ(mid_pos2, speed_J, acc)
    sleep(1)
    rtde_c.moveJ(mid_pos3, speed_J, acc)
    sleep(1)
    rtde_c.moveJ(mid_pos4, speed_J, acc)
    sleep(1)
    # open gripper
    move_fingers_to_position(open_pos)
    rtde_c.moveJ(grasp_raspberry, speed_J, acc)
    # close gripper
    move_fingers_to_position(close_pos)
    pose = rtde_r.getActualTCPPose()
    pose[2] = pose[2] - 0.05
    rtde_c.moveL(pose, speed_L, acc)
    # rtde_c.moveJ(pull_raspberry, speed, acc)
    sleep(4)
    rtde_c.moveJ(grasp_raspberry, speed_J, acc)
    sleep(2.5)
    # open gripper
    move_fingers_to_position(open_pos)
    rtde_c.moveJ(initial, speed_J, acc)


if __name__ == '__main__':
    main()

