'''
Notes:

+- 8cm from the center in each dimension
x-side -
y-fwd +
z-up +

'''

import os
import json
import socket
from threading import Thread
from time import sleep, time
import matplotlib.pyplot as plt

import cv2 as cv
import depthai as dai
import numpy as np

from harvest import Harvest
from PID_Gains import *
from reset_robot import Reset_Robot
from vision import Vision

from libraries.keyboard import Key


from libraries.comms_wrapper import Arduino

# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *



key = Key()

experimentID = 'demo'#input('\n\n\nID number for current experiment: ')
print('\n\n\n')

vis   = Vision(experimentID)
harv  = Harvest(experimentID)
reset = Reset_Robot()

    
# Plotjuggler communication
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
plot_juggler_port = 9871
newData = {"zero":0}

start_joints = [0.5187795162200928, -1.0295751730548304, -2.3532496134387415, 0.25610923767089844, 1.8091161251068115, 0.0030072915833443403]



def move_offset(start_joints, start_offset):

    reset.move_arm_joints(start_joints,0.1)

    input("press enter to offset starting location")

    motion_offst = [start_offset["side"], start_offset["up"], start_offset["fwd"]] #[0, -0.045, 0]
    R = reset.get_R06('UR5')
    ur_pose = reset.change_arm(R@motion_offst, 1)
    reset.move_arm_to_position(ur_pose, 0.04)

def align_stereo_cam(align_thresh):
    vis.chose_raspberry()

    input("press enter to move arm")
    # vis.maxRadius = 60
    # qRgb = vis.open_stereo_cam()
    pipeline, _ = vis.set_camera_pipeline()
    with dai.Device(pipeline) as device:
        # Output queue will be used to get the rgb frames from the output defined above
        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        # Skip the first frames while the camera focuses
        count = 0
        while count < 20:
            _  = qRgb.get().getCvFrame() 
            count += 1
        # Align x and y
        for i in range(2):
            aligned = False
            actual_speed = [0, 0, 0]
            align_speed = [0, 0, 0]
            while not aligned:
                aligned, direction = vis.align_frame(qRgb, i, align_thresh)
                
                if direction == 'move left':
                    align_speed = [-0.01, 0, 0]
                elif direction == 'move right':
                    align_speed = [0.01, 0, 0]  
                elif direction == 'move up':
                    align_speed = [0, -0.01, 0]
                elif direction == 'move down':
                    align_speed = [0, 0.01, 0]  
                
                print("align speed", align_speed)
                if actual_speed != align_speed:
                    reset.stop_arm()
                    sleep(0.5)

                    print("\n\n\n STOP ARM", actual_speed, align_speed, (actual_speed != align_speed))

                    actual_speed = align_speed
                    R = reset.get_R06('UR5')
                    actual_speed_for_arm = R@actual_speed
                    pos = reset.change_arm(actual_speed_for_arm, 0)
                    reset.move_arm(pos)
                print(direction)
            reset.stop_arm()

def approach_rasp_depth():
    depth = 1000
    move_closer = [0, 0, 0.05] #m
    i = 0
    while depth > 300:
        raspberries = vis.find_raspberry_coordinate()
        
        vis.save_video_data(vis.depthMap, "disparity_map"+str(i), (vis.w_adj, vis.h_adj))
        vis.save_video_data(vis.raspDetection, "detected_raspberries"+str(i), (vis.w_adj, vis.h_adj))
        vis.save_video_data(vis.rgb_frames, "rgb_frame"+str(i), (vis.w_rgb, vis.h_rgb))
        i += 1

        centre_rasp, _= vis.centre_raspberry(raspberries, [0,0])
        depth = centre_rasp[2]
        if depth <= 300: #mm
            move_closer = [0, 0, 0.1] #m
        R = reset.get_R06('UR5')
        ur_pose = reset.change_arm(R@move_closer, 1)
        reset.move_arm_to_position(ur_pose)

def approach_rasp_CHT():
    maxRadius = 40
    approach_rasp_CHT_frames = []
    # qRgb = vis.open_stereo_cam()
    pipeline, _ = vis.set_camera_pipeline()
    with dai.Device(pipeline) as device:
        # Output queue will be used to get the rgb frames from the output defined above
        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        # Skip the first frames while the camera focuses
        count = 0
        while count < 20:
            _  = qRgb.get().getCvFrame() 
            count += 1
        vis.verified_radius = 0
        move_forward = [0, 0, 0.01]
        R = reset.get_R06('UR5')
        pos = reset.change_arm(R@move_forward, 0)
        reset.move_arm(pos)
        while vis.verified_radius < maxRadius:
            rasp_coord, radii, centre, frame= vis.get_rasp_pixel_coord(qRgb, False)
            approach_rasp_CHT_frames.append(frame)
            centre_rasp,_ = vis.centre_raspberry(rasp_coord, centre)
            _ = vis.verify_raspberry(centre_rasp, 0, radii)
            print('Radius',vis.verified_radius)
            # vis.param2 += 1 # change parameteer of CHT as the robot arm moves forward
        reset.stop_arm()
        vis.save_video_data(approach_rasp_CHT_frames, "Approach_rasp_CHT", (frame.shape[1], frame.shape[0]))

def test_gripper():
    harv.move_fingers_to_position(harv.open)
    sleep(0.5)
    harv.move_fingers_to_position(harv.mid_pos)
    sleep(0.5)
    harv.move_fingers_to_position(harv.open)
    sleep(0.5)


def main(start_offset):
    # input("press enter to start moving gripper")
    
    # test_gripper()

    input("Press enter to move arm to initial position")
    
    # Position the arm takes to detect the raspberries
    # NOTE: MIGHT NEED TO BE CHANGED ON THE FIELD 
    
    harv.move_fingers_to_position(harv.open)

    pulling_force_init = 0
    time_stamps = []
    # lowest_z = 0.11720719872012049

    zeroTime = time()

    move_offset(start_joints, start_offset)

    input('\n\nPress Enter to align')
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Align raspberry with OAK-D RGB camera
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    align_stereo_cam(align_thresh = 5)
    vis.save_video_data(vis.align_frames, "Aligning_w_raspberry", vis.size_align_frames)
    time_stamps.append(time() - zeroTime)

    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Depth detection 
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    input('\n\nPress Enter to approach')
    time_stamps.append(time() - zeroTime)
    harv.move_fingers_to_position(harv.close)
    # approach_rasp_depth()    
    approach_rasp_CHT()  

    input("\n\n press enter to align fingers")  

    harv.move_fingers_to_position(harv.mid_pos)
    sleep(0.5)
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Align with the fingers
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    move_up = [0, -0.050, 0] #[0, -0.045, 0]
    R = reset.get_R06('UR5')
    ur_pose = reset.change_arm(R@move_up, 1)
    reset.move_arm_to_position(ur_pose, 0.12)
    
    time_stamps.append(time() - zeroTime)
    
    input('\n\nPress Enter to approach with TOF') 

    sleep(0.5)
    time_stamps.append(time() - zeroTime)
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Approach raspberry using TOF sensor
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    
    while 1:
        approached = harv.approach_raspberry()

        sleep(0.05)
        if key.keyPress =="q":
            break
        
    approach_speed = [0, 0,  0.01] #0.005]
    R = reset.get_R06('UR5')
    pos = reset.change_arm(R@approach_speed, 0)
    reset.move_arm(pos)

    # previous = []
    while not approached:
        approached = harv.approach_raspberry()
        # if msg != previous:
        #     print(msg)
        #     previous = msg
        # if msg[1] == 1:
        #     break

    reset.stop_arm()
    sleep(0.5)

    # pos = [0.06148506700992584, -1.9372103849994105, -1.0775254408465784, -0.08519059816469365, 1.4849560260772705, -0.01267558733095342]
    # reset.move_arm_joints(pos)

    before_fwd = reset.get_joints()
    print(before_fwd)

    move_forward = [0, 0, 0.025] # 32
    R = reset.get_R06('UR5')
    pos = reset.change_arm(R@move_forward, 1)
    reset.move_arm_to_position(pos)

    # sleep(0.5)
    # move_up = [0, -0.005, 0]
    # R = reset.get_R06('UR5')
    # ur_pose = reset.change_arm(R@move_up, 1)
    # reset.move_arm_to_position(ur_pose, 0.025)

    time_stamps.append(time() - zeroTime)

    print('\n\nTime to approach raspberry [s]: ', time() - zeroTime)


    input('\n\nPress Enter to grasp and pull raspberry') 
    time_stamps.append(time() - zeroTime)

if __name__ == '__main__':
    #main()
    loop_count = 0
    for z in range(3):
        for x in range(3):
            for y in range(3):
                loop_count+= 1

                if loop_count > 8:
                    print("\n\n\n",x, y, z, "count:", loop_count)
                    input("enter to go to start")

                    increment = 0.05
                    offset = {"up":y*increment, "side":x*(-increment), "fwd":z*increment}
                    # move_offset(start_joints, offset)
                    main(offset)