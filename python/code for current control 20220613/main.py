# Import stuff...
from harvest import Harvest
from arm_parametres import *
from reset_robot import Reset_Robot
from update import Update_Parametres
from evaluation import Evaluate_Pick
import matplotlib.pyplot as plt

from time import time


def main():
    ### Initialization values----------------------------
    ## ON STEM 
    # PID Gains
    Kp1 = 0.1
    Ki1 = 0.005
    Kd1 = 0.002

    previous_error = 0
    error_sum      = 0

    # Initial desired gripping force
    F_d1 = 350

    # Approching currrent
    app_cur = -10

    # Grasped force
    grasp_force = 60

    ## OFF STEM 
    # PID Gains
    Kp2 = Kp1#0.3
    Ki2 = Ki1#0.01
    Kd2 = Kd1#0

    # Initial desired gripping force
    F_d2 = 70
    #----------------------------------------------------

    # Total number of iterations
    n = 30

    file = open('Training Parametres.txt', 'w+')

    raspberry = np.array([0.44718335130836045, 0.005694612773243091, 0.19373339759857385, -1.2471996942876111, 1.1169595961006917, -1.1425923716038846])

    ref_l = "python/human_harvest_loadcell.csv"
    ref_p = "python/human_harvest_raspberry.csv"

    pull_vec = np.array([0, 0, -0.03])  #m np.array([delta_x, delta_y, delta_z])

    harv  = Harvest()
    reset = Reset_Robot()
    eval  = Evaluate_Pick()
    upd   = Update_Parametres()

    # output = 2
    # while np.abs(output) > 1:
    #     output = eval.get_data()

    ref_loadcell = eval.get_reference(ref_l)
    ref_pressure = eval.get_reference(ref_p)

    reset.move_arm(raspberry, speed_L, acc_L, False)
    
    # Begin training -----------------------------------------
    iter = 1
    zeroTime = time()
    while iter <= n:
        eval.take_offset()
        output = 2
        while np.abs(output) > 1:
            output = eval.get_data()

        check_stem = True
        harvest_data = []
        pulling_force = []
        compression_loadcell = []

        Fd1 = upd.init_Fd(ref_pressure, F_d1)   
        Fd2 = upd.init_Fd(ref_pressure, F_d2)  
        Fd = Fd1 

        #---------------------------------------------------
        # Grasp and pull
        #---------------------------------------------------
        # Approach raspberry
        compression_force = harv.get_compression_force()
        while abs(compression_force) < grasp_force:
            harv.move_gripper(app_cur)
            compression_force = harv.get_compression_force()
            previous_error = compression_force - Fd[0]
            error_sum += compression_force - Fd[0]
            harvest_data.append(eval.get_data())
            pulling_force.append(eval.get_vertical_force())
            t = time() - zeroTime
            eval.plot_data(compression_force, ref_pressure[0], grasp_force, eval.get_data(), eval.get_vertical_force(), harv.get_gripper_vertical_force(), t)
        harv.move_gripper(0)
        
        harv.set_controller(Kp1, Ki1, Kd1, previous_error, error_sum)

        # Grasp Raspberry
        error = 16        
        harvest_data.append(eval.get_data())
        while np.abs(error) > 15:
            error, compression_force, _ = harv.control_gripper(Fd[0], False)
            compression_loadcell.append(compression_force)
            harvest_data.append(eval.get_data())
            pulling_force.append(eval.get_vertical_force())
            t = time() - zeroTime
            eval.plot_data(compression_force, ref_pressure[0], Fd[0], eval.get_data(), eval.get_vertical_force(), harv.get_gripper_vertical_force(), t)
        harv.move_gripper(0)       

        # Pull Raspberry
        pos = reset.get_final_pos(pull_vec)
        reset.move_arm(pos, speed_L, acc_L, True)

        # Hold harvested Raspberry
        i = 1
        stop = Fd.shape[0]
        while i < stop:
            error = 12
            while np.abs(error) > 10:
                error, compression_force, action = harv.control_gripper(Fd[i], eval.is_off_stem())
                if eval.is_off_stem() and check_stem:
                    # if iter == 1:
                    i_off_stem = i
                    Fd[i::] = Fd2[i::]
                    check_stem = False 
                    stop = i_off_stem + 50
                    print('OFF STEM '+str(i))
                    harv.set_controller(Kp2, Ki2, Kd2, 0, 0)   

                if np.abs(reset.get_z_robot() - pos[2]) < 0.00005 and harv.is_closed() and action == 0:
                    stop = i
                    break

                harvest_data.append(eval.get_data())
                compression_loadcell.append(compression_force)
                pulling_force.append(eval.get_vertical_force())
                t = time() - zeroTime
                eval.plot_data(compression_force, ref_pressure[i], Fd[i], eval.get_data(), eval.get_vertical_force(), harv.get_gripper_vertical_force(), t)
            i += 1
        harv.move_gripper(0)

        #-----------------------------------------------------------------------------------------------
        # Check if raspberry was picked successfully
        #-----------------------------------------------------------------------------------------------
        verify = []
        verify.append(harv.get_compression_force())
        while not harv.is_closed():
            harv.move_gripper(10)
            verify.append(harv.get_compression_force())
        harv.move_gripper(0)
        verify.append(harv.get_compression_force())

        if (verify[-1]-verify[0]) > 10:
            check_stem = False
            error = 12
            while np.abs(error) > 10:
                error, _, _ = harv.control_gripper(Fd[-1], False)
            harv.move_gripper(0) 
        else:
            check_stem = True 


        if not check_stem:
            Fd = Fd[0:stop]
            #---------------------------------------------------
            # Analyse data
            #---------------------------------------------------
            pull_data, ref_pull = eval.process_data(harvest_data, pulling_force, ref_loadcell, ref_pressure, 1)
            release_data, ref_release = eval.process_data(harvest_data, pulling_force, ref_loadcell, ref_pressure, 2)
            
            y_dif = [ref_pull[ref_pull.shape[0]-5:ref_pull.shape[0]],ref_release[0:5]]
            y_hat_dif = [pull_data[pull_data.shape[0]-5:pull_data.shape[0]],release_data[0:5]]
            
            pull_loss, dif_pull = eval.cost_function(ref_pull, pull_data, y_dif, y_hat_dif)
            print('Pulling loss = '+str(pull_loss))

            y_dif = ref_release[ref_release.shape[0]-10:ref_release.shape[0]]
            y_hat_dif = release_data[release_data.shape[0]-10:release_data.shape[0]]

            release_loss, dif_release = eval.cost_function(ref_release, release_data, y_dif)
            print('Release loss = '+str(release_loss))


            #---------------------------------------------------
            # Register Parametres
            #---------------------------------------------------
            file.write('\n\n\n')
            file.write('Iteration = '+str(iter)+'\n')
            file.write('Fd = '+str(Fd)+'\n')
            file.write('Compression Loadcell = '+str(compression_loadcell)+'\n')
            file.write('Pulling Force = '+str(pulling_force)+'\n')
            file.write('Raspberry Reading = '+str(harvest_data)+'\n')
            file.write('Pull controller: K = ['+str(Kp1)+', '+str(Ki1)+', '+str(Kd1)+']\n')
            file.write('Release controller: K = ['+str(Kp2)+', '+str(Ki2)+', '+str(Kd2)+']\n')
            file.write('Pulling loss = '+str(pull_loss)+'\n')
            file.write('Pulling force difference = '+str(dif_pull)+'\n')
            file.write('Release loss = '+str(release_loss)+'\n')
            file.write('Release force difference = '+str(dif_release)+'\n')
            file.write('#############################################################################')
            

            print('\n\n\n')
            print('Iteration = '+str(iter)+'\n')
            print('Fd = '+str(Fd)+'\n')
            print('Compression loadcell reading = '+str(compression_loadcell)+'\n')
            print('Pull controller: K = ['+str(Kp1)+', '+str(Ki1)+', '+str(Kd1)+']')
            print('\nRelease controller: K = ['+str(Kp2)+', '+str(Ki2)+', '+str(Kd2)+']\n')
            print('#############################################################################')
            
            #---------------------------------------------------
            # Update parametres
            #---------------------------------------------------
            F_d1 = upd.update_Fd(ref_pull, pull_data, F_d1, 1, dif_pull)
            F_d2 = upd.update_Fd(ref_release, release_data, F_d2, 2, dif_release)
            Kp1, Ki1 = upd.update_controller(Kp1, Ki1, F_d1, 1)
            Kp2, Ki2 = upd.update_controller(Kp2, Ki2, F_d2, 2)
        else:
            #---------------------------------------------------
            # Register Parametres
            #---------------------------------------------------
            file.write('\n\n\n')
            file.write('Iteration = '+str(iter)+'\n')
            file.write('PICKING FAILED\n')
            file.write('#############################################################################')
            

            print('\n\n\n')
            print('Iteration = '+str(iter)+'\n')
            print('PICKING FAILED\n')
            print('##################################################################################')
            F_d1 = upd.update_Fd(ref_pull, pull_data, F_d1, 1, dif_pull)
            F_d1 += 100
            if F_d1 > 350:
                F_d1 = 350
            Kp1, Ki1 = upd.update_controller(Kp1, Ki1, F_d1, 1)
            
        
        #---------------------------------------------------
        # Reset raspberry and robot to position
        #---------------------------------------------------
        reset.move_arm(raspberry, speed_L, 0.1, False)
        while not harv.is_open():
            harv.move_gripper(10)
        harv.move_gripper(0)
        harv.reset()
        eval.reset()
        iter += 1
        print('\n\n\n\n NEW ITERATION \n\n\n\n')

    file.close()


if __name__ == '__main__':
    main()
