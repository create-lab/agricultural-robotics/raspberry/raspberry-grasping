## ON STEM #####################################
# PID Gains
Kp1 = 0.1
Ki1 = 0.0005
Kd1 = 0.002

# Initial desired gripping force
F_d1 = 350

# Approching currrent
app_cur = -10

# Grasped force
grasp_force = 60

## OFF STEM ###################################
# PID Gains
Kp2 = Kp1#0.3
Ki2 = Ki1#0.01
Kd2 = Kd1#0

# Initial desired gripping force
F_d2 = 70