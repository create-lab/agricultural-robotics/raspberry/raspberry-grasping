import sys
from arm_parametres import IP_robot

import rtde_receive
import rtde_control

from yaml import load
sys.path.append('libraries/')
from arm_parametres import IP_robot


class Reset_Robot():
    def __init__(self):
        self.rtde_c = rtde_control.RTDEControlInterface(IP_robot)
        self.rtde_r = rtde_receive.RTDEReceiveInterface(IP_robot)


    def move_arm(self, pos, speed, acc, flag):
        self.rtde_c.moveL(pos, speed, acc, flag)


    def get_final_pos(self, pull_vec):
        pos = self.rtde_r.getActualTCPPose()
        pos[0] += pull_vec[0]
        pos[1] += pull_vec[1]
        pos[2] += pull_vec[2]
        return pos


    def get_z_robot(self):
        return self.rtde_r.getActualTCPPose()[2]


