import numpy as np



class Update_Parametres():
    def __init__(self):
        self.alpha       = 61.91   # update parametre for Fd
        self.Fd_max      = 350
        self.previuos_Fd = 0
        self.beta_p      = 0.0005  # update parametre for Kp
        self.beta_i      = 0.000005 # update parametre for Ki


    def init_Fd(self, reference, Fd_0):
        return np.ones_like(reference)*Fd_0


    def update_Fd(self, reference, F_rasp_hat, Fd):
        # return Fd - self.alpha*(reference - F_rasp_hat)
        self.previuos_Fd = np.ones_like(Fd)*Fd
        print('\n\n\n\n\n')
        print(self.previuos_Fd)
        Fd += self.alpha*(reference - F_rasp_hat)
        Fd = np.where(Fd > 0 , Fd, 0)
        return np.where(Fd < self.Fd_max, Fd, self.Fd_max)


    def update_controller(self, Kp, Ki, Fd, i_off_stem, controller):
        # ON STEM
        if controller == 1:
            dif = np.mean(self.previuos_Fd[0:i_off_stem] - Fd[0:i_off_stem])
            print(self.previuos_Fd[0:i_off_stem])
            print(Fd[0:i_off_stem])
            print(self.previuos_Fd[0:i_off_stem] - Fd[0:i_off_stem])
        # OFF STEM
        elif controller == 2:
            dif = np.mean(self.previuos_Fd[i_off_stem+1::] - Fd[i_off_stem+1::])
        
        Kp += dif * self.beta_p
        Ki += dif * self.beta_i       

        if Kp < 0:
            Kp = 0.1
        
        print('Kp = '+str(Kp)+' Ki = '+str(Ki)+'\ndif = '+str(dif))
        return Kp, Ki


    





