import sys
from arm_parametres import IP_robot

import rtde_receive
import rtde_control

from yaml import load
sys.path.append('libraries/')
from arm_parametres import IP_robot

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key


class Reset_Robot():
    def __init__(self):
        self.rtde_c = rtde_control.RTDEControlInterface(IP_robot)
        self.rtde_r = rtde_receive.RTDEReceiveInterface(IP_robot)


    def move_arm(self, pos, speed, acc):
        self.rtde_c.moveL(pos, speed, acc, True)


    def get_final_pos(self, pull_vec):
        pos = self.rtde_r.getActualTCPPose()
        pos[0] += pull_vec[0]
        pos[1] += pull_vec[1]
        pos[2] += pull_vec[2]
        return pos


    def get_z_robot(self):
        return self.rtde_r.getActualTCPPose()[2]


