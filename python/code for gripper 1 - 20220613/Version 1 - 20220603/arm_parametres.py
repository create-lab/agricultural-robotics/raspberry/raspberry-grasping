import numpy as np
import math

## Robotic arm parametres
# IP address
IP_robot = "192.168.1.20"

# Home position
start_pos    = [0, -90.00 , -90.00, 0, 90.00, 0]

# Speeds and accelerations - Joint space
speed_J = 0.1
acc_J   = 0.1

# Speeds and accelerations - Cartesian space
speed_L = 0.1
acc_L   = 0.1

# Parameters for camera to UR3 coordinate transformation
# camera position on the UR3 referencial
p_cam        = np.array([[0.04959681137681763], 
                          [0.2682097722067104],
                                         [0.2]]) #[m]

# angle between camera referencial and UR3 referencial
phi          = 20*math.pi/180 

# Rotation matrix
R            = np.array([[np.sin(phi), np.cos(phi), 0], 
                                             [0, 0, 1], 
                        [np.cos(phi), -np.sin(phi), 0]]) 

# camera z measurement systematic error
z_offset     = np.array([[0],[0],[0.17]]) 

# systematic error after transformation
robot_offset = np.array([[0],[0.06],[0.03]]) 
