import csv
from time import time

from libraries.comms_wrapper import Arduino

# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *

import json
import socket



class Evaluate_Pick():
    def __init__(self):
        self.raspberrySensors = Arduino(descriptiveDeviceName="Pressure and pulling sensor arduino", portName="COM6", baudrate=115200)
        
        self.raspberrySensors.connect_and_handshake()

        offset = find_offset(self.raspberrySensors, ["pressure1"])[0]
        print("The baseline threshold is: ", offset)
        self.s1 = Fluidic_Sensor(offset)

        self.zeroTime = time()
        self.raspberrySensors.receive_message(printOutput=False)

        # Plotjuggler
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
        self.plot_juggler_port = 9871   
        # Define what data to send to plotjuggler
        self.newData = {
            "zero":0
        }


    def get_vertical_force(self):
        force = 0
        self.raspberrySensors.receive_message()
        if self.raspberrySensors.newMsgReceived:
            force = np.abs(float(self.raspberrySensors.receivedMessages['lc3']))
        return force
        # else:
        #     return 0


    def is_off_stem(self):
        pass


    def get_data(self):     
        stop = False  
        while not stop:
            self.raspberrySensors.receive_message(printOutput=False) 
            if self.raspberrySensors.newMsgReceived:  
                t = time() - self.zeroTime
                raw = float(self.raspberrySensors.receivedMessages["pressure1"])

                # Perform data processing and auto drift compensation
                #s1.auto_drift_compensation(raw, t)
                self.s1.smooth_signal(raw, t)
                output, _ = self.s1.get_processed_signal()
                stop = True
                return output


    def get_reference(self, filename):
        harvest_ref = []
        rows = []
        # reading csv file
        with open(filename, 'r') as csvfile:
            # creating a csv reader object
            csvreader = csv.reader(csvfile)

            # extracting each data row one by one
            for row in csvreader:
                rows.append(row)

        harvest_ref = [float(x) for x in rows[0]]
        return np.array(harvest_ref)


    def process_data(self, harvest_data, reference):
        harvest_ref = self.get_reference(reference)
        index = np.where(np.floor(np.abs(harvest_data)) > 0)
        if index[0].shape[0] == 0:
            index = np.where(harvest_data == np.max(harvest_data))
        new_data = harvest_data[index[0][0]::]        
        if new_data.shape[0] < harvest_ref.shape[0]:
            new_data = np.append(new_data, [0]*(harvest_ref.shape[0]-new_data.shape[0]))
            #flag = 0
        elif new_data.shape[0] > harvest_ref.shape[0]:
            harvest_ref = np.append(harvest_ref, [0]*(new_data.shape[0]-harvest_ref.shape[0]))
            #flag = 1
        return new_data, harvest_ref#, flag


    def cost_function(self, y, y_hat):
        """Mean squared error"""
        return (1/len(y))*np.sum((y-y_hat)**2)


    def get_cost(self, harvest_data, reference):
        y_hat, y = self.process_data(harvest_data, reference)
        return self.cost_function(y, y_hat)


    def plot_data(self, compression_force, F_d, harvest_ref, rasp_output, pulling_force):
        self.newData["gripping force"]         = compression_force
        self.newData["desired gripping force"] = F_d
        # self.newData["control action"]         = action
        self.newData["reference"]              = harvest_ref
        self.newData["raspberry"]              = rasp_output
        self.newData["pulling force"]          = pulling_force

        # Send to plot juggler            
        self.sock.sendto(json.dumps(self.newData).encode(), ("127.0.0.1", self.plot_juggler_port))  



    
        
