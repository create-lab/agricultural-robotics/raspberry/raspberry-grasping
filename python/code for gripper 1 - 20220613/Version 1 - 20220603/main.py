# Import stuff...
from PID_Gains import *
from harvest import Harvest
from arm_parametres import *
from reset_robot import Reset_Robot
from update import Update_Parametres
from evaluation import Evaluate_Pick

from time import time


def main():

    raspberry = np.array([0.44718335130836045, 0.005694612773243091, 0.17773339759857385, -1.2471996942876111, 1.1169595961006917, -1.1425923716038846])
    n = 10 # total number of iterations
    iter = 1

    Kp1 = 0.1
    Ki1 = 0.0
    Kd1 = 0.0

    reference = "python/average human harvest 3.csv"

    pull_vec = np.array([0, 0, -0.03])  #m np.array([delta_x, delta_y, delta_z])

    harv = Harvest()
    reset = Reset_Robot()
    eval = Evaluate_Pick()
    upd = Update_Parametres()

    output = 2
    while np.abs(output) > 1:
        output = eval.get_data()

    harvest_ref = eval.get_reference(reference)
    Fd1 = upd.init_Fd(harvest_ref, F_d1)   
    Fd2 = upd.init_Fd(harvest_ref, F_d2)  
    Fd = Fd1 
    
    reset.move_arm(raspberry, speed_L, acc_L)

    # Begin training -----------------------------------------
    while iter <= n:
        check_stem = True
        harv.set_controller(Kp1, Ki1, Kd1)
        harvest_data = []

        #---------------------------------------------------
        # Grasp and pull
        #---------------------------------------------------
        error = 2
        harvest_data.append(eval.get_data())
        while np.abs(error) > 1:
            harvest_data.append(eval.get_data())
            error, compression_force = harv.control_gripper(Fd[0])
            pulling_force = eval.get_vertical_force()
            eval.plot_data(compression_force, Fd[0], harvest_ref[0], eval.get_data(), pulling_force)
        harv.stop_gripper()

        pos = reset.get_final_pos(pull_vec)
        reset.move_arm(pos, speed_L, acc_L)

        i = 1
        while i < Fd.shape[0]:
            error = 2
            while np.abs(error) > 1:
                if harv.is_off_stem() and check_stem:
                    if iter == 1:
                        Fd[i::] = Fd2[i::]
                    harv.set_controller(Kp2, Ki2, Kd2)
                    i_off_stem = i
                    print('OFF STEM'+str(i))
                    check_stem = False                
                error, compression_force = harv.control_gripper(Fd[i])
                pulling_force = eval.get_vertical_force()
                eval.plot_data(compression_force, Fd[i], harvest_ref[i], eval.get_data(), pulling_force)
            harvest_data.append(eval.get_data())
            i += 1
        harv.stop_gripper()

        #---------------------------------------------------
        # Analyse data
        #---------------------------------------------------
        harvest_array = np.array(harvest_data)
        cost = eval.get_cost(harvest_array, reference)
        print('loss = '+str(cost))

        #---------------------------------------------------
        # Update parametres
        #---------------------------------------------------
        print(Fd)
        Fd = upd.update_Fd(harvest_ref, harvest_array, Fd)
        print('\n\n\n\n\n')
        print(Fd)
        Kd1, Ki1 = upd.update_controller(Kp1, Ki1, Fd, i_off_stem, 1)

        #---------------------------------------------------
        # Reset raspberry and robot to position
        #---------------------------------------------------
        reset.move_arm(raspberry, speed_L, 0.1)
        while not harv.is_open():
            harv.move_gripper(-10)
        harv.move_gripper(0)
        harv.reset()
        iter += 1


if __name__ == '__main__':
    main()
