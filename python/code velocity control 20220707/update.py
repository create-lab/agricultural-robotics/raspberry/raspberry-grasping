import numpy as np



class Update_Parametres():
    def __init__(self):
        self.alpha                  = 25 #61.91# update parametre for Fd
        self.Fd_max                = 390
        self.Fd_min                 = 10
        self.beta_pg                = 0.0001   # update parametre for Kp grasp
        self.beta_pp                = 0.001    # update parametre for Kp pull
        self.beta_i                 = 0.00000 # update parametre for Ki
        self.previuos_successful_Fd = [400]

    def init_Fd(self, reference, Fd_0):
        return np.ones_like(reference)*Fd_0


    def update_Fd(self, Fd, dif, n_fails, n_trials):
        # return Fd - self.alpha*(reference - F_rasp_hat)
        ## ON STEM
        Fd_max = self.Fd_max
        if n_fails != 0:
            i = -1
            while self.previuos_successful_Fd[i] == Fd:
                i += -1
            Fd1 = Fd + np.abs(Fd-self.previuos_successful_Fd[i])* n_fails/n_trials
            self.previuos_Fd = Fd
            if Fd1 > Fd_max:
                Fd1 = Fd_max
            return Fd1
        else:
            self.previuos_successful_Fd.append(Fd)
            self.previuos_Fd = Fd
            if dif > 0:
                Fd = Fd - 10
                if Fd < self.Fd_min:
                    Fd = self.Fd_min
                return Fd
            else:
                Fd = Fd + self.alpha*dif
                if Fd > Fd_max:
                    Fd = Fd_max
                return Fd


    def update_controller(self, Kp, Fd, controller, dif2=0):
        ## grasp
        if controller == 1:
            dif = np.mean(self.previuos_Fd - Fd)
            Kp += dif* self.beta_pg
        ## pull
        elif controller == 2:
            dif = dif2
            Kp += dif * self.beta_pp
        
        print('Kp = '+str(Kp)+' dif = '+str(dif)+' Fd = '+str(Fd))
        return Kp


    





