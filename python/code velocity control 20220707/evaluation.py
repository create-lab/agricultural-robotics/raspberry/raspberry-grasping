import csv
from time import time
from scipy.cluster.vq import kmeans2, whiten, cdist

from libraries.comms_wrapper import Arduino

# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *

import json
import socket



class Evaluate_Pick():
    def __init__(self):
        self.previous_force          = 0
        self.previous_verticalForce  = 0
        self.off_stem_thresh         = 70#100

        self.raspberrySensors = Arduino(descriptiveDeviceName="Pressure and pulling sensor arduino", portName="COM6", baudrate=115200)
        
        self.raspberrySensors.connect_and_handshake()

        # offset = find_offset(self.raspberrySensors, ["pressure1"])[0]
        # print("The baseline threshold is: ", offset)
        self.s1 = 0

        self.zeroTime = time()
        self.raspberrySensors.receive_message(printOutput=False)

        # Plotjuggler
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
        self.plot_juggler_port = 9871   
        # Define what data to send to plotjuggler
        self.newData = {
            "zero":0
        }
    
    def take_offset(self):
        offset = find_offset(self.raspberrySensors, ["pressure1"])[0]
        print("The baseline threshold is: ", offset)
        self.s1 = Fluidic_Sensor(offset)

    def reset(self):
        self.previous_force          = 0
        self.previous_verticalForce  = 0

    def get_vertical_force(self):
        self.raspberrySensors.receive_message()
        if self.raspberrySensors.newMsgReceived:
            force = np.abs(float(self.raspberrySensors.receivedMessages['lc3']))
            self.previous_verticalForce = force
            return force
        else:
            return self.previous_verticalForce

    def is_off_stem(self):
        force = self.get_vertical_force()
        if abs(self.previous_force - force) > self.off_stem_thresh:
            # self.previous_force = force
            return True
        else:
            self.previous_force = force
            return False

    def get_data(self):     
        stop = False  
        while not stop:
            self.raspberrySensors.receive_message(printOutput=False) 
            if self.raspberrySensors.newMsgReceived:  
                t = time() - self.zeroTime
                raw = float(self.raspberrySensors.receivedMessages["pressure1"])

                # Perform data processing and auto drift compensation
                #s1.auto_drift_compensation(raw, t)
                self.s1.smooth_signal(raw, t)
                output, _ = self.s1.get_processed_signal()
                stop = True
                # output = raw
                return output

    def get_reference(self, filename):
        harvest_ref = []
        rows = []
        # reading csv file
        with open(filename, 'r') as csvfile:
            # creating a csv reader object
            csvreader = csv.reader(csvfile)
            # extracting each data row one by one
            for row in csvreader:
                rows.append(row)
        harvest_ref = [float(x) for x in rows[0]]
        return np.array(harvest_ref)

    ## NOTE USE IN CASE WE COMPARE A VECTOR OF THE HIGHEST VALUES ##
    # def get_max_values(self, harvest_data, ref):
    #     harv_array = np.array(harvest_data)
    #     max_val = np.max(harv_array)
    #     max_val_vec = harv_array[np.where(np.abs(harv_array-max_val) <= 0.5)]
    #     if ref.shape[0] > max_val_vec.shape[0]:
    #         ref = ref[0:max_val_vec.shape[0]]
    #     elif ref.shape[0] < max_val_vec.shape[0]:
    #         max_val_vec = max_val_vec[0:ref.shpe[0]]
    #     return max_val_vec, ref     

    # def process_compare_data(self, n_trials, ref):
    #     for trial in n_trials:
    #         trial = trial[0:ref.shpae[0]]
    #     return n_trials

    # through standard deviation
    def clip_outliers(self, mean_vals, std_limit):
        """ Clip outliers bigger than a certain standard deviation limit (std_limit)"""
        ## NOTE USE IN CASE WE COMPARE A VECTOR OF THE HIGHEST VALUES ##
        # mean_vals = np.mean(np.array(n_trials), axis=1) 
        std = np.std(mean_vals)
        mean = np.mean(mean_vals)
        mean_vals[mean_vals > mean + std*std_limit] = mean + std*std_limit
        mean_vals[mean_vals < mean - std*std_limit] = mean - std*std_limit
        return mean_vals  

    def cost_function(self, y, y_hat):
        """Mean squared error"""
        # MSE = (1/len(y))*np.sum((y-y_hat)**2)
        dif = np.mean(y-y_hat)
        relative_error = np.mean((y-y_hat)/y)
        return dif, relative_error#,MSE
        # return np.mean(y[y.shape[0]-10:y.shape[0]]-y_hat[y_hat.shape[0]-10:y_hat.shape[0]])

    def plot_data(self, compression_force, F_d, rasp_output, pulling_force, pulling_gripper, t):#harvest_ref,
        self.newData["gripping force"]           = compression_force
        self.newData["desired gripping force"]   = F_d
        # self.newData["control action"]           = action
        # self.newData["reference"]                = harvest_ref
        self.newData["raspberry"]                = rasp_output
        self.newData["pulling force"]            = pulling_force
        self.newData["pulling gripper"]          = pulling_gripper
        self.newData['timestamp']                = t

        # Send to plot juggler            
        self.sock.sendto(json.dumps(self.newData).encode(), ("127.0.0.1", self.plot_juggler_port))  

    # def harvesting_phases(self, trial):
    #     dif = []
    #     for i,value in enumerate(trial):
    #         if i > 0:
    #             dif.append(value - trial[i-1])
    #     pulling = [dif.index(max(dif))-1, dif.index(min(dif))+1]
    #     release = [pulling[1], pulling[1]*2 - pulling[0]]
    #     return pulling, release

    # def process_data(self, harvest_data, pulling_force, ref_loadcell, ref_pressure, phase):
    #     pull_ref, release_ref = self.harvesting_phases(ref_loadcell)
    #     reference1 = np.array(ref_pressure[pull_ref[0]:pull_ref[1]])       # Pulling phase
    #     reference2 = np.array(ref_pressure[release_ref[0]:release_ref[1]]) # Release Phase

    #     pull, release = self.harvesting_phases(pulling_force)
    #     pull_data = np.array(harvest_data[pull[0]:pull[1]])               # Pulling phase
    #     release_data = np.array(harvest_data[release[0]:release[1]])      # Release Phase

    #     pull_compare = np.array(harvest_data[0:pull[1]])
    #     release_compare = np.array(harvest_data[release[0]::])

    #     if phase == 1:   # Pulling phase
    #         delta_i = np.abs(reference1.shape[0]-pull_data.shape[0]) 
    #         if reference1.shape[0] < pull_data.shape[0]:
    #             begin = pull_ref[0]-delta_i
    #             if begin < 0:
    #                 begin = 0
    #             harvest_ref = ref_pressure[begin:pull_ref[0]]
    #             harvest_ref = np.append(harvest_ref, reference1)
    #             new_data = pull_data[0:harvest_ref.shape[0]]
                
    #         elif reference1.shape[0] > pull_data.shape[0]:
    #             begin = pull[0]-delta_i
    #             if begin < 0:
    #                 begin = 0
    #             new_data = harvest_data[begin:pull[0]]
    #             new_data = np.append(new_data, pull_data)
    #             harvest_ref = reference1[0:new_data.shape[0]]
        
    #     elif phase == 2: # Release Phase
    #         delta_i = np.abs(reference2.shape[0]-release_data.shape[0]) 
    #         if reference2.shape[0] < release_data.shape[0]:
    #             end = release_ref[1]+delta_i+1
    #             if end > len(ref_pressure):
    #                 end = len(ref_pressure)
    #             harvest_ref = ref_pressure[release_ref[1]+1:end]
    #             harvest_ref = np.append(reference2, harvest_ref)
    #             new_data = release_data[0:harvest_ref.shape[0]]
                
    #         elif reference2.shape[0] > release_data.shape[0]:
    #             end = release[1]+delta_i+1
    #             if end > len(harvest_data):
    #                 end = len(harvest_data)
    #             new_data = harvest_data[release[1]+1:end]
    #             new_data = np.append(release_data, new_data)
    #             harvest_ref = reference2[0:new_data.shape[0]]
    #     return new_data, harvest_ref, pull_compare, release_compare

    # def pulling_comparison(self, pull_data, release_data, points):
    #     max_pull = np.max(pull_data)
    #     max_release = np.max(release_data)
    #     if max_release >= max_pull:
    #         y_hat_dif = release_data[0:np.argwhere(release_data == max_release)[0][0]+2] # ♥
    #         dif_len = np.abs(points - y_hat_dif.shape[0])
    #         if y_hat_dif.shape[0] < points:
    #             y_hat_dif = np.append(pull_data[pull_data.shape[0]-dif_len::], y_hat_dif)
    #         elif y_hat_dif.shape[0] > points:
    #             y_hat_dif = y_hat_dif[dif_len::]
    #     elif max_pull > max_release:
    #         y_hat_dif = release_data[np.argwhere(pull_data == max_pull)[0][0]-2::]
    #         dif_len = np.abs(points - y_hat_dif.shape[0])
    #         if y_hat_dif.shape[0] < points:
    #             y_hat_dif = np.append(y_hat_dif, release_data[0:dif_len])
    #         elif y_hat_dif.shape[0] > points:
    #             y_hat_dif = y_hat_dif[dif_len::]

    #     print('y_hat_dif_pull = '+str(y_hat_dif))
    #     return y_hat_dif


    # def release_comparison(self, release_data, points):
    #     data_min = release_data.tolist().index(min(release_data))
    #     y = release_data[data_min::]
    #     if len(y) > points:
    #         y = y[0:points]
    #     elif len(y) < points:
    #         y = np.append(release_data[data_min-(points-len(y)):data_min], y)
    #     return y


    # def get_cost(self, harvest_data, pulling_force, ref_loadcell, ref_pressure, phase):
    #     y_hat, y = self.process_data(harvest_data, pulling_force, ref_loadcell, ref_pressure, phase)
    #     return self.cost_function(y, y_hat)




    
        
