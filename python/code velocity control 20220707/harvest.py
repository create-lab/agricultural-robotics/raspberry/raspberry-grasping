import sys
import numpy as np

from PID_Gains import *

from yaml import load
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key

import json
import socket
from time import time


class Harvest():
    def __init__(self):
        self.previous_error       = 0
        self.error_sum            = 0
        self.open                 = 1000#1005#5100
        self.close                = -300
        self.Kp                   = 0
        self.Ki                   = 0
        self.Kd                   = 0
        self.previous_force       = 0
        self.check_stem           = True
        self.previous_compression = 0
        self.previous_pulling     = 0
        self.off_stem_thresh      = 30
        self.max_vel              = 300
        self.approach_thresh      = 50
        self.approaching_speed    = 20
        
        # Dynamixel
        self.gripper = dynamixel(ID=112, descriptive_device_name="Gripper", series_name="xm", baudrate=1000000, port_name="COM4")
        self.gripper.begin_communication()
        self.gripper.set_operating_mode("velocity")

        self.loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
        self.loadcell.connect_and_handshake()

    def set_controller(self, Kp, Ki, Kd):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.previous_error = 0
        self.error_sum      = 0

    def reset(self):
        self.previous_force = 0
        self.check_stem = True

    def is_off_stem_gripper(self):
        force = self.get_gripper_vertical_force()
        if self.previous_force - force > self.off_stem_thresh:
            return True
        else:
            self.previous_force = force
            return False

    # Approach raspberry
    def grasp_raspberry(self):
        while self.get_compression_force() < self.approach_thresh:
            self.move_gripper(-self.approaching_speed)
        self.move_gripper(0)

    def open_fingers(self):
        while not self.is_open():
            self.move_gripper(self.approaching_speed)
        self.move_gripper(0)

    def PID_gripper(self, error):
        """ PID controller for the gripper """
        self.error_sum += error
        error_dif = error - self.previous_error
        self.previous_error = error

        return self.Kp*error + self.Ki*self.error_sum - self.Kd*error_dif


    def get_compression_force(self):
        self.loadcell.receive_message()
        if self.loadcell.newMsgReceived:
            force = np.abs(float(self.loadcell.receivedMessages['lc2']))
            self.previous_compression = force
        else:
            force = self.previous_compression
        return force


    def get_gripper_vertical_force(self):
        self.loadcell.receive_message()
        if self.loadcell.newMsgReceived:
            force = np.abs(float(self.loadcell.receivedMessages['lc1']))
            self.previous_pulling = force
        else:
            force = self.previous_pulling
        return force


    def control_gripper(self, F_d, is_off_stem):
        compression_force = 0
        while compression_force == 0:
            compression_force = self.get_compression_force()

        error = compression_force - F_d
            
        if is_off_stem and self.check_stem:
            self.set_controller(Kp2, Ki2, Kd2)
            self.check_stem = False
            return error, compression_force, 0
    
        action = self.PID_gripper(error)
        if action > self.max_vel:
            action = self.max_vel
        if action < 0 and self.is_closed():
            action = 0
        if self.is_open() and action > 0:
            action = 0
        self.move_gripper(action)
        return error, compression_force, action


    def check_raspberry(self):
        verify = []
        verify.append(self.get_compression_force())
        while self.gripper.read_position() > 0:
            self.move_gripper(-10)
            verify.append(self.get_compression_force())
        self.move_gripper(0)
        verify.append(self.get_compression_force())
        return verify


    def move_gripper(self, action):
        self.gripper.write_velocity(action)


    def is_open(self):
            if self.gripper.read_position() >= self.open:
                return True
            else:
                return False


    def is_closed(self):
        if self.gripper.read_position() <= self.close:
            return True
        else:
            return False
            



    ## Position control
    # def grasp_raspberry(self, F_d):
    #     self.loadcell.receive_message()

    #     if self.loadcell.newMsgReceived:
    #         compression_force = np.abs(float(self.loadcell.receivedMessages['lc2']))
    #         error = F_d - compression_force
    #         while np.abs(error) > 1:
    #             action = self.PID_gripper(error) + self.gripper.read_position()
    #             if action >= self.open and action <= self.close:
    #                 self.gripper.write_position(int(action))


    # def grasp_raspberry(self, F_d, sock, newData, plot_juggler_port, harvest_ref, rasp_output):#, pull, t, zeroTime):
    #     error = 2
    #     while np.abs(error) > 1:
    #         compression_force = self.get_compression_force()
            
    #         if self.is_off_stem() and self.check_stem:
    #             self.set_controller(Kp2, Ki2, Kd2)
    #             self.check_stem = False
    #             break
            
    #         error = F_d - compression_force
    #         action = self.PID_gripper(error)
    #         if action > 260:
    #             action = 260
    #         if action > 0 and self.gripper.read_position() >= self.close:
    #             action = 0
    #         if self.gripper.read_position() <= self.open and action < 0:
    #             action = 0
    #         self.move_gripper(action)
            
    #         newData["gripping force"] = compression_force
    #         newData["desired gripping force"] = F_d
    #         newData["control action"] = action
    #         newData["reference"] = harvest_ref
    #         newData["raspberry"] = rasp_output
    #         newData["pulling force"] = 
        

    #         # Send to plot juggler            
    #         sock.sendto(json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port))                
    #     self.gripper.write_velocity(0)