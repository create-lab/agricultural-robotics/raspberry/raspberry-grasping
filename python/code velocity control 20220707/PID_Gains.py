## ON STEM 
# PID Gains
Kp1 = 0.1
Ki1 = 0
Kd1 = 0

# Initial desired gripping force
F_d1 = 350

## OFF STEM 
# PID Gains
Kp2 = 0.4
Ki2 = 0.0001
Kd2 = 0

# Initial desired gripping force
F_d2 = 10