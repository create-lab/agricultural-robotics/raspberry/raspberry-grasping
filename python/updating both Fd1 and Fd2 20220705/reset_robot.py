import sys
from arm_parametres import IP_robot

import rtde_receive
import rtde_control

from yaml import load
sys.path.append('libraries/')
from arm_parametres import IP_robot


class Reset_Robot():
    def __init__(self):
        self.rtde_c = rtde_control.RTDEControlInterface(IP_robot)
        self.rtde_r = rtde_receive.RTDEReceiveInterface(IP_robot)

    def move_arm_to_position(self, pos, speed, acc, flag=False):
        self.rtde_c.moveL(pos, speed, acc, flag)

    def get_final_pos(self, pull_vec):
        pos = self.get_arm_speed()
        pos[0] += pull_vec[0]
        pos[1] += pull_vec[1]
        pos[2] += pull_vec[2]
        return pos

    def move_arm(self, cartesian_speed, acc=0.25):
        self.rtde_c.speedL(cartesian_speed, acc)

    def stop_arm(self, acc=10):
        self.rtde_c.speedStop(acc)

    def get_arm_speed(self):
        return self.rtde_r.getActualTCPSpeed()

    def get_arm_pose(self):
        return self.rtde_r.getActualTCPPose()


