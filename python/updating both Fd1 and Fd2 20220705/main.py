# Import stuff...
from turtle import position
from harvest import Harvest
from arm_parametres import *
from reset_robot import Reset_Robot
from update import Update_Parametres
from evaluation import Evaluate_Pick
from time import time, sleep


def main():
    ### Initialization values----------------------------
    ## ON STEM 
    # PID Gains
    Kp1 = 0.05
    Ki1 = 0
    Kd1 = 0

    # Initial desired gripping force
    F_d1 = 350

    ## OFF STEM 
    # PID Gains
    Kp2 = 0.25
    Ki2 = 0.0001
    Kd2 = 0

    # Initial desired gripping force
    F_d2 = 70
    #----------------------------------------------------

    # Total number of iterations
    n = 40

    file = open('Training Parametres.txt', 'w+')

    raspberry = np.array([0.40670533828941496, -0.09180054420682882, 0.2872483479130191, -1.192262946808976, 1.16311974859761, -1.0979691895928154])

    ref_l = "python/human_harvest_loadcell_4gWOBendy.csv"
    ref_p = "python/human_harvest_raspberry_4gWOBendy.csv"

    pull_vec = np.array([0, 0, -0.1])  #m np.array([delta_x, delta_y, delta_z])

    harv  = Harvest()
    reset = Reset_Robot()
    eval  = Evaluate_Pick()
    upd   = Update_Parametres()

    # output = 2
    # while np.abs(output) > 1:
    #     output = eval.get_data()

    ref_loadcell = eval.get_reference(ref_l)
    ref_pressure = eval.get_reference(ref_p)

    reset.move_arm_to_position(raspberry, speed_L, acc_L, False)
    
    # Begin training -----------------------------------------
    iter = 1
    while iter <= n:
        zeroTime = time()
        eval.take_offset()
        output = 2
        while np.abs(output) > 1:
            output = eval.get_data()

        check_stem = True
        harvest_data = []
        pulling_force = []
        compression_loadcell = []
        time_vec = []
        pulling_gripper= []

        harv.set_controller(Kp1, Ki1, Kd1)

        Fd1 = upd.init_Fd(ref_pressure, F_d1)   
        Fd2 = upd.init_Fd(ref_pressure, F_d2)  
        Fd = Fd1 

        #---------------------------------------------------
        # Grasp and pull
        #---------------------------------------------------
        # Approach and grasp raspberry
        error = 12        
        harvest_data.append(eval.get_data())
        while np.abs(error) > 10:
            error, compression_force, _ = harv.control_gripper(Fd[0], False)
            compression_loadcell.append(compression_force)
            harvest_data.append(eval.get_data())
            pulling_force.append(eval.get_vertical_force())
            t = time() - zeroTime
            time_vec.append(t)
            pulling_gripper.append(harv.get_gripper_vertical_force())
            eval.plot_data(compression_force, ref_pressure[0], Fd[0], eval.get_data(), eval.get_vertical_force(), harv.get_gripper_vertical_force(), t)
        harv.move_gripper(0) 
        sleep(0.5)       

        # Pull raspberry
        pos = reset.get_final_pos(pull_vec)
        #reset.move_arm_to_position(pos, speed_L, acc_L, True)
        reset.move_arm(pos)

        # Release and hold raspberry
        i = 1
        stop = Fd.shape[0]
        timer = time()
        count = 0
        while i < stop:
            error = 12
            while np.abs(error) > 10:
                error, compression_force, action = harv.control_gripper(Fd[i], eval.is_off_stem())
                if eval.is_off_stem() and check_stem:
                    # if iter == 1:
                    reset.stop_arm()
                    i_off_stem = i
                    Fd[i::] = Fd2[i::]
                    check_stem = False 
                    stop = i_off_stem + 50
                    print('OFF STEM '+str(i))
                    harv.set_controller(Kp2, Ki2, Kd2)   

                # if np.abs(reset.get_z_robot() - pos[2]) < 0.00005 and harv.is_closed() and action == 0:
                #     stop = i
                #     break

                # if np.abs(reset.get_arm_speed()[2]) < np.abs(pos[2]) and harv.is_closed() and action == 0:
                #     stop = i
                #     break
                
                count += 1
                time_vec.append(t)
                harvest_data.append(eval.get_data())
                compression_loadcell.append(compression_force)
                pulling_force.append(eval.get_vertical_force())
                pulling_gripper.append(harv.get_gripper_vertical_force())
                t = time() - zeroTime
                eval.plot_data(compression_force, ref_pressure[i], Fd[i], eval.get_data(), eval.get_vertical_force(), harv.get_gripper_vertical_force(), t)
            i += 1
        print((time() - timer )/count)
        harv.move_gripper(0)

        #-----------------------------------------------------------------------------------------------
        # Check if raspberry was picked successfully
        #-----------------------------------------------------------------------------------------------
        verify = harv.check_raspberry()

        print('\n\nverified\n\n')

        if (verify[-1]-verify[0]) > 10 and stop > 50:
            check_stem = False
            error = 12
            while np.abs(error) > 10:
                error, _, _ = harv.control_gripper(Fd[-1], False)
            harv.move_gripper(0) 
        else:
            check_stem = True 


        if not check_stem:
            Fd = Fd[0:stop]
            #---------------------------------------------------
            # Analyse data
            #---------------------------------------------------
            pull_data, ref_pull, pull_compare, release_compare = eval.process_data(harvest_data, pulling_force, ref_loadcell, ref_pressure, 1)
            release_data, ref_release, _, _ = eval.process_data(harvest_data, pulling_force, ref_loadcell, ref_pressure, 2)
            
            # y_dif = np.append(np.array(ref_pull[ref_pull.shape[0]-5::]),np.array(ref_release[0:5]))
            # y_hat_dif = np.array([pull_data[pull_data.shape[0]-5:pull_data.shape[0]],release_data[0:5]])
            points = 6
            y_hat_dif = eval.pulling_comparison(pull_compare, release_compare, points)
            y_dif = eval.pulling_comparison(np.array(ref_pull), np.array(ref_release), points)

            pull_loss, dif_pull, relative_error_pull = eval.cost_function(ref_pull, pull_data, y_dif, y_hat_dif)
            print('Pulling loss = '+str(pull_loss))

            # y_dif = eval.release_comparison(ref_release, points)
            y_hat_dif = harvest_data[len(harvest_data)-points::]
            y_dif = ref_pressure[ref_pressure.shape[0]-points::]

            release_loss, dif_release, relative_error_release = eval.cost_function(ref_release, release_data, y_dif, y_hat_dif)


            #---------------------------------------------------
            # Register Parametres
            #---------------------------------------------------
            file.write('\n\n\n')
            file.write('Iteration = '+str(iter)+'\n')
            file.write('Fd = '+str(Fd)+'\n')
            file.write('Compression Loadcell = '+str(compression_loadcell)+'\n')
            file.write('Time vector = '+str(time_vec)+'\n')
            file.write('Pulling Force = '+str(pulling_force)+'\n')
            file.write('Pulling Gripper = '+str(pulling_gripper)+'\n')
            file.write('Raspberry Reading = '+str(harvest_data)+'\n')
            file.write('Pull controller: K = ['+str(Kp1)+', '+str(Ki1)+', '+str(Kd1)+']\n')
            file.write('Release controller: K = ['+str(Kp2)+', '+str(Ki2)+', '+str(Kd2)+']\n')
            file.write('Pulling loss = '+str(pull_loss)+'\n')
            file.write('Pulling force relative error = '+str(relative_error_pull)+'\n')
            file.write('Pulling difference = '+str(dif_pull)+'\n')
            file.write('Release loss = '+str(release_loss)+'\n')
            file.write('Release force relative error = '+str(relative_error_release)+'\n')
            file.write('Release difference = '+str(dif_release)+'\n')
            file.write('#############################################################################')
            

            print('\n\n\n')
            print('Iteration = '+str(iter)+'\n')
            print('Fd = '+str(Fd)+'\n')
            print('Pull controller: K = ['+str(Kp1)+', '+str(Ki1)+', '+str(Kd1)+']')
            print('\nRelease controller: K = ['+str(Kp2)+', '+str(Ki2)+', '+str(Kd2)+']\n')
            print('##################################################################################')
            
            #---------------------------------------------------
            # Update parametres
            #---------------------------------------------------
            F_d1 = upd.update_Fd(F_d1, 1, dif_pull)
            F_d2 = upd.update_Fd(F_d2, 2, dif_release)
            Kp1, Ki1 = upd.update_controller(Kp1, Ki1, F_d1, 1)
            Kp2, Ki2 = upd.update_controller(Kp2, Ki2, F_d2, 2)

            #---------------------------------------------------
            # Reset raspberry and robot to position
            #---------------------------------------------------
            reset.move_arm_to_position(raspberry, speed_L, 0.1, False)
            while not harv.is_open():
                harv.move_gripper(10)
            harv.move_gripper(0)
            harv.reset()
            eval.reset()
            iter += 1
        else:
            #---------------------------------------------------
            # Register Parametres
            #---------------------------------------------------
            file.write('\n\n\n')
            file.write('Iteration = '+str(iter)+'\n')
            file.write('PICKING FAILED\n')
            file.write('#############################################################################')
            

            print('\n\n\n')
            print('Iteration = '+str(iter)+'\n')
            print('PICKING FAILED\n')
            print('##################################################################################')
            F_d1 += 50
            if F_d1 > 390:
                F_d1 = 390
            Kp1, Ki1 = upd.update_controller(Kp1, Ki1, F_d1, 1)
            
        
            #---------------------------------------------------
            # Reset raspberry and robot to position
            #---------------------------------------------------
            while not harv.is_open():
                harv.move_gripper(10)
            harv.move_gripper(0)
            reset.move_arm_to_position(raspberry, speed_L, 0.1, False)
            harv.reset()
            eval.reset()
            iter += 1
        print('\n\n\n\n NEW ITERATION \n\n\n\n')

    file.close()


if __name__ == '__main__':
    main()
