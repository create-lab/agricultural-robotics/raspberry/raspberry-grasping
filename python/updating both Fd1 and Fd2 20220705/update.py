import numpy as np



class Update_Parametres():
    def __init__(self):
        self.alpha        = 25 #61.91   # update parametre for Fd
        self.Fd1_max      = 390
        self.Fd2_max      = 120
        self.Fd_min       = 10
        self.previuos_Fd1 = 0
        self.previuos_Fd2 = 0
        self.beta_p       = 0.00005    # update parametre for Kp
        self.beta_i       = 0.0000001 # update parametre for Ki


    def init_Fd(self, reference, Fd_0):
        return np.ones_like(reference)*Fd_0


    def update_Fd(self, Fd, phase, dif):
        # return Fd - self.alpha*(reference - F_rasp_hat)
        ## ON STEM
        if phase == 1:
            self.previuos_Fd1 = Fd
            Fd_max = self.Fd1_max
            if dif > 0:
                Fd = Fd - 5
                if Fd < self.Fd_min:
                    Fd = self.Fd_min
                return Fd
        ## OFF STEM
        elif phase == 2:
            self.previuos_Fd2 = Fd
            Fd_max = self.Fd2_max
        # dif = np.mean(reference - raspberry_reading)
        Fd = Fd + self.alpha*dif
        if Fd < self.Fd_min:
            Fd = self.Fd_min
        elif Fd > Fd_max:
            Fd = Fd_max
        return Fd


    def update_controller(self, Kp, Ki, Fd, controller):
        ## ON STEM
        if controller == 1:
            dif = np.mean(self.previuos_Fd1 - Fd)#[0:i_off_stem] - Fd[0:i_off_stem])
        ## OFF STEM
        elif controller == 2:
            dif = np.mean(self.previuos_Fd2 - Fd)#[i_off_stem+1::] - Fd[i_off_stem+1::])
        
        Kp += dif * self.beta_p
        Ki += dif * self.beta_i       

        if Kp < 0.1:
            Kp = 0.1
        if Ki < 0:
            Ki = 0
        
        print('Kp = '+str(Kp)+' Ki = '+str(Ki)+' dif = '+str(dif))
        return Kp, Ki


    





