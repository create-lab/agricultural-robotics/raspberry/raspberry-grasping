import rtde_receive
import rtde_control

rtde_c = rtde_control.RTDEControlInterface("192.168.1.25")
rtde_r = rtde_receive.RTDEReceiveInterface("192.168.1.25")


while True:
    input()
    print(rtde_r.getActualTCPPose())
    currpos = rtde_r.getActualTCPPose().copy()
    print(currpos)
    currpos[2] += 0.01

    rtde_c.moveL(currpos, 0.01, 0.1)

    print(rtde_r.getActualTCPPose())
    input()


import os
import json
import socket
from threading import Thread
from time import sleep, time

import cv2 as cv
import depthai as dai
import numpy as np

from harvest import Harvest
from PID_Gains import *
from reset_robot import Reset_Robot
from vision import Vision

experimentID = input('\n\n\nID number for current experiment: ')
print('\n\n\n')

vis   = Vision(experimentID)
harv  = Harvest(experimentID)
reset = Reset_Robot()

while True:
    input()
    print(reset.rtde_r.getActualTCPPose())
    currpos = reset.rtde_r.getActualTCPPose().copy()
    print(currpos)
    currpos[2] += 0.01

    reset.rtde_c.moveL(currpos, 0.01, 0.1)

    print(reset.rtde_r.getActualTCPPose())
    input()
    
# Plotjuggler communication
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
plot_juggler_port = 9871
newData = {"zero":0}

#----------------------------------------------------------------------------------------------------------------------------------------------------------
# Slip detection communication
#----------------------------------------------------------------------------------------------------------------------------------------------------------
localIP              = "10.15.20.195"
localPort            = 20001
bufferSize           = 1024

# Create a datagram socket
s = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
# Bind to address and ip
s.bind((localIP, localPort))
msg = 'null'

def read_socket():
    global msg
    while 1:
        bytesAddressPair = s.recvfrom(bufferSize)
        msg_str = bytesAddressPair[0].decode('utf-8')
        msg = json.loads(msg_str)
 
t = Thread(target = read_socket)
t.daemon = True
t.start()

def print_slip():
    sleep(1)
    ref = msg[0]
    while True:
        if msg[0] != ref:
            if abs(msg[0] - ref) >= 5:
                print('slip')
            else:
                print('no slip')

def align_stereo_cam(align_thresh):
    vis.chose_raspberry()
    # vis.maxRadius = 60
    # qRgb = vis.open_stereo_cam()
    pipeline, _ = vis.set_camera_pipeline()
    with dai.Device(pipeline) as device:
        # Output queue will be used to get the rgb frames from the output defined above
        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        # Skip the first frames while the camera focuses
        count = 0
        while count < 20:
            _  = qRgb.get().getCvFrame() 
            count += 1
        # Align x and y
        for i in range(2):
            aligned = False
            actual_speed = [0, 0, 0]
            align_speed = [0, 0, 0]
            while not aligned:
                aligned, direction = vis.align_frame(qRgb, i, align_thresh)
                if direction == 'move left':
                    align_speed = [-0.01, 0, 0]
                elif direction == 'move right':
                    align_speed = [0.01, 0, 0]  
                elif direction == 'move up':
                    align_speed = [0, -0.01, 0]
                elif direction == 'move down':
                    align_speed = [0, 0.01, 0]  
                
                print("align speed", align_speed)
                if actual_speed != align_speed:
                    reset.stop_arm()
                    sleep(0.5)

                    print("\n\n\n STOP ARM", actual_speed, align_speed, (actual_speed != align_speed))

                    actual_speed = align_speed
                    R = reset.get_R06('UR5')
                    actual_speed_for_arm = R@actual_speed
                    pos = reset.change_arm(actual_speed_for_arm, 0)
                    reset.move_arm(pos)
                print(direction)
            reset.stop_arm()

def approach_rasp_depth():
    depth = 1000
    move_closer = [0, 0, 0.05] #m
    while depth > 300:
        raspberries = vis.find_raspberry_coordinate()
        centre_rasp, _= vis.centre_raspberry(raspberries, [0,0])
        depth = centre_rasp[2]
        if depth <= 300: #mm
            move_closer = [0, 0, 0.1] #m
        R = reset.get_R06('UR5')
        ur_pose = reset.change_arm(R@move_closer, 1)
        reset.move_arm_to_position(ur_pose)

def approach_rasp_CHT():
    maxRadius = 40
    # qRgb = vis.open_stereo_cam()
    pipeline, _ = vis.set_camera_pipeline()
    with dai.Device(pipeline) as device:
        # Output queue will be used to get the rgb frames from the output defined above
        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        # Skip the first frames while the camera focuses
        count = 0
        while count < 20:
            _  = qRgb.get().getCvFrame() 
            count += 1
        vis.verified_radius = 0
        move_forward = [0, 0, 0.01]
        R = reset.get_R06('UR5')
        pos = reset.change_arm(R@move_forward, 0)
        reset.move_arm(pos)
        while vis.verified_radius < maxRadius:
            rasp_coord, radii, centre, _= vis.get_rasp_pixel_coord(qRgb, False)
            centre_rasp,_ = vis.centre_raspberry(rasp_coord, centre)
            _ = vis.verify_raspberry(centre_rasp, 0, radii)
            print('Radius',vis.verified_radius)
            # vis.param2 += 1 # change parameteer of CHT as the robot arm moves forward
        reset.stop_arm()

def test_gripper():
    harv.move_fingers_to_position(harv.open)
    sleep(0.5)
    harv.move_fingers_to_position(harv.mid_pos)
    sleep(0.5)
    harv.move_fingers_to_position(harv.open)
    sleep(0.5)

def main():
    # # Position the arm takes to detect the raspberries
    # # NOTE: MIGHT NEED TO BE CHANGED ON THE FIELD 
    # start_joints = [0.8161766529083252, -0.8411205450641077, -2.3005085627185267, 0.006565093994140625, 1.5466278791427612, 3.5952674807049334e-05]
    # lowest_z = 0.11720719872012049

    zeroTime = time()
    # reset.move_arm_joints(start_joints)
    # test_gripper()
    # input('Press Enter to align')
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # # Align raspberry with OAK-D RGB camera
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # align_stereo_cam(align_thresh = 5)
    # vis.save_video_data(vis.align_frames, "Aligning_w_raspberry", vis.size_align_frames)

    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # # Depth detection 
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # input('\n\nPress Enter to approach')
    # harv.move_fingers_to_position(harv.close)
    # # approach_rasp_depth()    
    # approach_rasp_CHT()    

    # harv.move_fingers_to_position(harv.mid_pos)
    # sleep(0.5)
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # # Align with the fingers
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # move_up = [0, -0.07, 0]
    # R = reset.get_R06('UR5')
    # ur_pose = reset.change_arm(R@move_up, 1)
    # reset.move_arm_to_position(ur_pose)
    
    # input('\n\nPress Enter to approach with TOF')       


    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # # Approach raspberry using TOF sensor
    # #----------------------------------------------------------------------------------------------------------------------------------------------------------
    
    # approached = harv.approach_raspberry()
    # approach_speed = [0, 0,  0.005]
    # R = reset.get_R06('UR5')
    # pos = reset.change_arm(R@approach_speed, 0)
    # reset.move_arm(pos)
    # while not approached:
    #     approached = harv.approach_raspberry()
    # reset.stop_arm()
    # sleep(0.5)
    # move_forward = [0, 0, 0.025]
    # R = reset.get_R06('UR5')
    # pos = reset.change_arm(R@move_forward, 1)
    # reset.move_arm_to_position(pos)
    # input('\n\nPress Enter to grasp and pull raspberry') 
    
    #----------------------------------------------------------------------------------------------------------------------------------------------------------
    # Grasp and pull
    #----------------------------------------------------------------------------------------------------------------------------------------------------------s     
    # for debugging #
    # joints raspberry = [0.7063584923744202, -1.6583831946002405, -1.6437566916095179, 0.16852736473083496, 1.6586774587631226, 0.00222756783477962]
    # raspberry = reset.get_arm_pose() # keep position of the raspberry
    raspberry = [0.4595179414661958, 0.24324064044750388, 0.5372336708059533, -1.4781173777239716, 0.6033006775171664, -0.6139250621304135]
    reset.move_arm_to_position(raspberry, 0.05, 0.1, True)
    # harv.move_fingers_to_position(harv.mid_pos)

    check_stem = False
    detect_slip = False
    fail = False
    slip_ref = 0
    time_vec = []
    pulling_gripper = []
    compression_loadcell = []
    F_d1 = harv.set_grasping_force()
    Fd = F_d1
    pull_speed = [0, 0.005, 0] # pulling speed [0, 0.005, 0]
    harv.set_controller(K1[0], K1[1], K1[2]) 
    error_margin = 10
    error = 12

    while True:
        while np.abs(error) > error_margin:
            if detect_slip:
                print('Slip', msg)
            # Stop arm and readjust if slip is detected 
            # NOTE: RUN SLIP DETECTION IN PARALLEL
            if detect_slip and abs(msg[0]-slip_ref) >= 3:
                print('slipping')

                
                reset.rtde_c.speedStop(10)
                reset.rtde_c.stopL(10)
                
                #reset.stop_arm()
                sleep(1)
                harv.move_gripper(0)
                move_up = [0, 0, 0.01]

                input()
                print(reset.rtde_r.getActualTCPPose())
                currpos = np.copy(reset.rtde_r.getActualTCPPose())
                print(currpos)
                currpos[2] += 0.01

                reset.rtde_c.moveL(currpos, 0.01, 0.1)

                print(reset.rtde_r.getActualTCPPose())
                input()
                # R = reset.get_R06('UR5')
                # pos = reset.change_arm(move_up, 1)
                # reset.move_arm_to_position(pos, 0.01)
                # # if msg[1] == '0':
                # harv.move_fingers_to_position(harv.mid_pos)
                harv.change_grasping_force_1(is_off_stem=False)
                Fd = harv.pulling_force[-1]
                print('Fd', Fd)
                detect_slip = False
                check_stem = False 
                error_margin = 10
                
            # Check when raspberry gets picked
            if harv.is_off_stem_gripper() and check_stem:
                reset.stop_arm()
                Fd = F_d2
                error_margin = 3
                check_stem = False 
                detect_slip = False
                print('OFF STEM')
                harv.set_controller(K2[0], K2[1], K2[2])   
                # harv.change_grasping_force_1(is_off_stem=True)
                # vis.close_raspi_cam()
        
            error, compression_force, _ = harv.control_gripper(Fd, harv.is_off_stem_gripper(),compression_loadcell)
            t = time() - zeroTime
            time_vec.append(t)
            compression_loadcell.append(compression_force)
            pulling_gripper.append(harv.get_gripper_vertical_force())

            # Plotjuggler
            newData["gripping force"]           = compression_force
            newData["desired gripping force"]   = Fd
            newData["pulling gripper"]          = harv.get_gripper_vertical_force()
            newData['timestamp']                = t
            sock.sendto(json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port))
            
            # If the picking is not detected
            if np.abs(reset.get_arm_pose()[2]-raspberry[2]) >= 0.05 and Fd == F_d1:
            # if abs(reset.get_arm_pose()[2]-lowest_z) >= 0.01 and Fd == F_d1:
                harv.move_gripper(0)
                reset.stop_arm()
                fail = True
                break
            
            # if slipping is not detected
            elif Fd == F_d2 and harv.is_in_position(harv.close):
                fail = True
                break
        if fail:
            print('\n\n\n Fail \n\n\n')
            break
        sock.sendto(json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port))
        
        # Pull raspberry
        if np.abs(error) <=  error_margin and Fd == harv.pulling_force[-1]:
            sleep(0.5)
            error_margin = 0
            R = reset.get_R06('UR5')
            pos = reset.change_arm(R@pull_speed, 0)
            reset.move_arm(pos)
            slip_ref = msg[0]
            detect_slip = True
            check_stem = True
            # start getting slip readings

        # Stop if raspberry was picked
        elif np.abs(error) <=  error_margin and Fd == F_d2:
            break
    sock.sendto(json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port))
    harv.move_gripper(0)
    # harv.save_csv_data(compression_loadcell, 'Compression_force_gripper')
    # harv.save_csv_data(pulling_gripper, 'Pulling_force_gripper')
    # harv.save_csv_data(time_vec, 'Time_vector')


if __name__ == '__main__':
    main()




#### OLD ####
# # Aligning gripper using Raspberry Pi camera
    # align_speed = np.zeros(3)
    # # opening camera
    # vis.open_raspi_cam()
    # count = 0
    # while count < 20:
    #     _ = vis.raspi_cam_frame()
    #     count += 1
    # min_dist, closest_rasp, center_frame = vis.align_raspberries()
    # x_dist = (center_frame[0] - closest_rasp[0])/center_frame[0]
    # y_dist = (center_frame[1] - closest_rasp[1])/center_frame[1]
    # align_speed[idx[0]] = 0.03*x_dist
    # align_speed[idx[1]] = 0.03*y_dist
    # pos = reset.change_arm(align_speed, 0)
    # reset.move_arm(pos)
    # while min_dist > vis.align_thresh:
    #     min_dist, _, _ = vis.align_raspberries()
    # reset.stop_arm()
    # vis.close_raspi_cam()
