from turtle import position
from harvest import Harvest
from arm_parametres import *
from reset_robot import Reset_Robot
from update import Update_Parametres
from evaluation import Evaluate_Pick
from time import time, sleep

import socket
import json

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871

def main():
    # Define what data to send to plotjuggler
    newData = {
        "zero":0
    }
    ### Initialization values----------------------------
    ## ON STEM 
    # PID Gains
    Kp1 = 0.1
    Ki1 = 0
    Kd1 = 0

    # Initial desired gripping force
    F_d1 = 200

    ## OFF STEM 
    # PID Gains
    Kp2 = 0.25
    Ki2 = 0.0001
    Kd2 = 0

    # Initial desired gripping force
    #F_d2 = 70
    F_d2 = 10
    #----------------------------------------------------
    
    n = 5

    harv  = Harvest()
    reset = Reset_Robot()
    eval  = Evaluate_Pick()
    upd   = Update_Parametres()

    raspberry = [0.3613504691383168, -0.08724524851539274, 0.23462936657237787, -1.1853374496659983, 1.1648386035450509, -1.1123410819704973]#reset.get_arm_pose()

    eval.take_offset()
    output = 2
    while np.abs(output) > 1:
        output = eval.get_data()

    iter = 0
    zeroTime = time()
    printTimer = time()
    file = open('raspberry_validation_constantCtrl_'+str(iter)+'_'+str(F_d1)+'_'+str(F_d2)+'.txt', 'w+')
    file.write('    time    ||     output     \n\n')
    closed = False
    while iter < n:
        reset.move_arm_to_position(raspberry, 0.07, acc_L, True)

        t = time() - zeroTime
        check_stem = True
        harv.set_controller(Kp1, Ki1, Kd1)
            
        output = eval.get_data()
        if not closed:
            file.write('      %f' % t+'       %f' % output+' \n')
        newData["output"] = output 
        newData["force"] = 0
        # Send to plot juggler            
        sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 

        error = 12        
        while np.abs(error) > 10:
            error, compression_force, _ = harv.control_gripper(F_d1, False)
            output = eval.get_data()
            if not closed:
                file.write('      %f' % t+'       %f' % output+' \n')
            newData["output"] = output 
            newData["force"] = compression_force
            # Send to plot juggler            
            sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) )
        harv.move_gripper(0) 
        pos = raspberry.copy()
        pos[2] = pos[2]-0.05
        reset.move_arm_to_position(pos, 0.05, acc_L, True)
        # output = eval.get_data()
        # if not closed:
        #     file.write('      %f' % t+'       %f' % output+' \n')
        # newData["output"] = output 
        # # Send to plot juggler            
        # sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) )
        error = 12
        Fd = F_d1
        while int(t) < 20:
            while np.abs(error) > 10:
                error, compression_force, action = harv.control_gripper(Fd, eval.is_off_stem())
                print(eval.is_off_stem())
                if eval.is_off_stem() and check_stem:
                    Fd = F_d2
                    check_stem = False 
                    harv.set_controller(Kp2, Ki2, Kd2)  
                    print('OFF') 
                output = eval.get_data()
                if not closed:
                    file.write('      %f' % t+'       %f' % output+' \n')
                newData["output"] = output 
                newData["force"] = compression_force
                # Send to plot juggler            
                sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) )   
            harv.move_gripper(0)  
            print(eval.is_off_stem())
            if eval.is_off_stem() and check_stem:
                Fd = F_d2#[iter]
                check_stem = False 
                harv.set_controller(Kp2, Ki2, Kd2)  
                print('OFF') 
                error = 12
            output = eval.get_data()
            if not closed:
                file.write('      %f' % t+'       %f' % output+' \n')
            newData["output"] = output 
            newData["force"] = compression_force
            # Send to plot juggler            
            sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) )              
            t = time() - zeroTime 
        harv.move_gripper(0)

        #print(s1.offset_adjustment)
        if time() - printTimer > 1:
            print(int(t), "seconds since start of loop")
            printTimer = time()

        if int(t) >= 20 and not closed:
            file.close()
            closed = True
            print('closed')
            iter += 1
            while not harv.is_open():
                harv.move_gripper(10)
            harv.move_gripper(0)
            harv.reset()
            eval.reset()
            zeroTime = time()
            printTimer = time()
            eval.take_offset()
            output = 2
            while np.abs(output) > 1:
                output = eval.get_data()
            file = open('raspberry_validation_constantCtrl_'+str(iter)+'_'+str(F_d1)+'_'+str(F_d2)+'.txt', 'w+')
            file.write('    time    ||     output     \n\n')
            closed = False

if __name__ == '__main__':
    main()
