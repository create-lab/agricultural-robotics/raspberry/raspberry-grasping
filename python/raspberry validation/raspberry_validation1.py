from time import sleep, time
import numpy as np
import sys

from yaml import load
sys.path.append('libraries/')

# Dynamixel libraries
from libraries.dynamixel_controller import dynamixel
from libraries.dynamixel_address_book import *
from libraries.comms_wrapper import Arduino
from libraries.keyboard import Key
import json
import socket
from Control import Controller

# fluidic sensor libraries
from fluidic_sensor.fluidic_sensor_class import Fluidic_Sensor
from fluidic_sensor.utility import *
from fluidic_sensor.config import *

# Online libraries
import socket
from time import time
import json
from sklearn.linear_model import LinearRegression
import numpy as np
from evaluation import Evaluate_Pick
from harvest import Harvest


sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
plot_juggler_port = 9871

gripper = dynamixel(ID=112, descriptive_device_name="Gripper", series_name="xm", baudrate=1000000, port_name="COM4")
gripper.begin_communication()
gripper.set_operating_mode("position")

loadcell = Arduino(descriptiveDeviceName="loadcell arduino", portName="COM5", baudrate=115200)
loadcell.connect_and_handshake()

def main():
    # Define what data to send to plotjuggler
    newData = {
        "zero":0
    }
    n = 10
    ## Dynamixel max positions
    open_pos = 400
    pos = 40

    eval = Evaluate_Pick()

    eval.take_offset()
    output = 2
    while np.abs(output) > 1:
        output = eval.get_data()

    iter = 1
    zeroTime = time()
    printTimer = time()
    file = open('raspberry_validation_'+str(iter)+'.txt', 'w+')
    file.write('    time    ||     output     \n\n')
    closed = False
    while iter <= n:

        t = time() - zeroTime

        gripper.write_position(pos)
            
        output = eval.get_data()
        if not closed:
            file.write('      %f' % t+'       %f' % output+' \n')
        newData["output"] = output 

        # Send to plot juggler            
        sock.sendto( json.dumps(newData).encode(), ("127.0.0.1", plot_juggler_port) ) 

        #print(s1.offset_adjustment)
        if time() - printTimer > 1:
            print(int(t), "seconds since start of loop")
            printTimer = time()

        if int(t) > 30 and not closed:
            file.close()
            closed = True
            print('closed')
            iter += 1
            gripper.write_position(open_pos)
            sleep(0.5)
            zeroTime = time()
            printTimer = time()
            eval.take_offset()
            output = 2
            while np.abs(output) > 1:
                output = eval.get_data()
            file = open('raspberry_validation_'+str(iter)+'.txt', 'w+')
            file.write('    time    ||     output     \n\n')
            closed = False

if __name__ == '__main__':
    main()
