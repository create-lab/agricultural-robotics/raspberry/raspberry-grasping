from vision import Vision
from reset_robot import Reset_Robot
import numpy as np
from time import sleep


picking_joints = [0.09517721831798553, -1.6912196318255823, -1.4978888670550745, -3.0695303122149866, -1.464867893849508, 0.03478571027517319]
start_joints = [0.10294412076473236, -0.5206435362445276, -1.9580839315997522, -3.79954177538027, -1.5440385977374476, 0.023521663621068]

vis = Vision()
reset = Reset_Robot()

reset.move_arm_joints(start_joints, 0.3, 0.7)

raspberries = vis.find_raspberry_coordinate()
print(raspberries)

centre_rasp = vis.choose_raspberry(raspberries)

R = reset.get_R06('UR3')
p6 = reset.get_arm_pose()
ur_coord_rasp = vis.cam_to_UR3(centre_rasp, R, p6[0:3]).tolist()

reset.move_arm_joints(picking_joints, 0.3, 0.7)
sleep(0.5)
ur_pose = reset.get_arm_pose()
print(ur_pose[1] - ur_coord_rasp[1][0])
print(ur_pose[2] - ur_coord_rasp[2][0])

# ur_pose[0] = ur_coord_rasp[0][0]
ur_pose[1] = ur_coord_rasp[1][0]
ur_pose[2] = ur_coord_rasp[2][0]

print('\n\nraspberry in cam coordinate: '+str(centre_rasp))
print('raspberry in ur3 coordinate: '+str(ur_coord_rasp))

reset.move_arm_to_position(ur_pose, 0.05, 0.7)