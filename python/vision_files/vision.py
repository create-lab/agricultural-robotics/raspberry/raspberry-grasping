import os
import math
import cv2 as cv
import numpy as np
import depthai as dai
from datetime import datetime
from scipy.cluster.vq import kmeans2, whiten
from scipy.spatial.distance import cdist
from gen2_calc_spatials_on_host.utility import *
from gen2_calc_spatials_on_host.calc import HostSpatialsCalc


class Vision():
    def __init__(self):
        # Blur kernel
        self.n_blur       = 9
        # Hue threshold
        self.thresh       = 160#150 
        # Circle Hough Transform Parameters
        self.minDist      = 30
        self.param1       = 100
        self.param2       = 14   ########################################################
        self.minRadius    = 1    # NOTE: FIND THE PARAMETRES FOR THE RASPBERRY APPROACH #
        self.maxRadius    = 50   ########################################################
        # RGB window
        self.w_rgb        = 640
        self.h_rgb        = 400
        self.interLeaved  = False
        # Camera window offset
        self.y_init       = 60
        self.x_init       = 50#55
        self.h_adj        = 300
        self.w_adj        = 480
        self.scale        = self.w_adj/self.w_rgb
        # Stereo Parametres
        self.LR_check     = False
        self.subPixel     = False
        self.extDisparity = True
        # Define how many frames will be analysed and the initial delay 
        self.cameraOn     = 400
        self.delay        = 150
        # Region of interest margin
        self.delta        = 5
        # Parameters for camera to UR3 coordinate transformation
        ## Camera position in frame 6
        self.pcam6 = np.array([[0], [0.055], [0.125]])
        ## camera z measurement systematic error
        self.z_offset     = np.array([[0],[0],[0]]) #0.17
        self.robot_offset = np.array([[0],[0],[0]]) #0 0.05 0.08
        # Percentile for outlier removal
        self.percentile = 90
        # Directory to save data
        self.directory = 'data/Real raspberry trials'
        #Slip detection memory 
        self.slip_thresh = 10
        self.previous_frame = 255
        self.detect_slip    = False

    def rescale_frame(self, frame):
        """This function resizes the the input image (frame) by a certain scale"""
        width = int(frame.shape[1]*self.scale)
        height = int(frame.shape[0]*self.scale)
        dimensions = (width, height)
        return cv.resize(frame, dimensions, interpolation = cv.INTER_AREA)

    def find_circles(self, image, img_process):
        """Apply circle Hough transform"""
        # source: https://docs.opencv.org/3.4/d4/d70/tutorial_hough_circle.html
        circles = cv.HoughCircles(img_process, cv.HOUGH_GRADIENT, 1, self.minDist,
                                    param1=self.param1, param2=self.param2,
                                    minRadius=self.minRadius, maxRadius=self.maxRadius)
        rasp_coord = []
        rasp_window = []
        max_rad = 0
        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                center = (i[0], i[1])
                # circle center
                cv.circle(image, center, 1, (255, 0, 0), 3)
                # circle outline
                radius = i[2]
                coord = (i[0]-radius, i[1]-radius, i[0]+radius, i[1]+radius)
                cv.circle(image, center, radius, (255, 0, 255), 3)
                rasp_coord.append(center)
                rasp_window.append(coord)
                if radius > max_rad:
                    max_rad = radius
        return rasp_coord, rasp_window, image

    def get_rasperries(self, image):
        """Find raspberries in frame"""
        image = self.rescale_frame(image)
        image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        hue = image_hsv[:,:,0] 
        image_gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY) 
        new_hue = np.zeros_like(hue)
        new_hue[np.where(hue>self.thresh)] = image_gray[np.where(hue>self.thresh)]# + bias
        blur_rasps = cv.GaussianBlur(new_hue,(self.n_blur,self.n_blur), 0)            
        rasp_coord, rasp_window, found_raspberries = self.find_circles(image, blur_rasps)
        return rasp_coord, rasp_window, found_raspberries

    def set_raspicam_pipeline(self,
        sensor_id=0,
        capture_width=1920,
        capture_height=1080,
        display_width=960,
        display_height=540,
        framerate=30,
        flip_method=0,
    ):
        return (
            "nvarguscamerasrc sensor-id=%d !"
            "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
            "nvvidconv flip-method=%d ! "
            "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
            "videoconvert ! "
            "video/x-raw, format=(string)BGR ! appsink"
            % (
                sensor_id,
                capture_width,
                capture_height,
                framerate,
                flip_method,
                display_width,
                display_height,
            )
        )

    def open_raspi_cam(self):
        self.video_capture = cv.VideoCapture(self.set_raspicam_pipeline(flip_method=0), cv.CAP_GSTREAMER)

    def raspi_cam_frame(self):
        if self.video_capture.isOpened():
            _, frame = self.video_capture.read()
            return frame

    def close_raspi_cam(self):
        self.video_capture.release()

    # def align_raspberries(self):
    #     frame = self.raspi_cam_frame()                                              
    #     _,_,_,rasps = self.get_rasperries(frame) 
    #     rasp_coord, _, found_raspberries = self.find_circles(frame, rasps)
    #     cv.imshow("centering", found_raspberries)
    #     distances =[]
    #     centre = [int(frame.shape[1]/2), int(frame.shape[0]/2)]
    #     for elem in rasp_coord:
    #         # cdist is used to calculate the distance between center and other points
    #         distances = np.append(distances, cdist(centre,elem, 'euclidean'))
    #     return np.min(distances), rasp_coord[np.where(distances == np.min(distances))], centre

    def slip_detection(self):
        image = self.raspi_cam_frame()   
        image = self.rescale_frame(image)
        image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        hue = image_hsv[:,:,0] 
        binary_mask = np.zeros_like(hue)
        binary_mask[np.where(hue>self.thresh)] = 255
        rasps = cv.GaussianBlur(binary_mask,(self.n_blur,self.n_blur), 0)        
        slipping = self.previous_frame - np.mean(rasps)
        self.previous_frame =  np.mean(rasps)
        if slipping < self.slip_thresh:
            return False
        else:
            return True

    def set_camera_pipeline(self):                                                                                      
        """Create pipeline for OAK-D camera"""
        # Create pipeline
        pipeline = dai.Pipeline()
        
        # Define source and output
        camRgb = pipeline.create(dai.node.ColorCamera)
        monoLeft = pipeline.create(dai.node.MonoCamera)
        monoRight = pipeline.create(dai.node.MonoCamera)
        stereo = pipeline.create(dai.node.StereoDepth)

        # Properties
        camRgb.setPreviewSize(self.w_rgb, self.h_rgb)
        camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        camRgb.setInterleaved(self.interLeaved)
        camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

        monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
        monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

        stereo.initialConfig.setConfidenceThreshold(255)
        stereo.setLeftRightCheck(self.LR_check)
        stereo.setSubpixel(self.subPixel)
        stereo.setExtendedDisparity(self.extDisparity)

        # Linking
        monoLeft.out.link(stereo.left)
        monoRight.out.link(stereo.right)

        xoutRgb = pipeline.create(dai.node.XLinkOut)
        xoutRgb.setStreamName("rgb")
        camRgb.preview.link(xoutRgb.input)

        xoutDepth = pipeline.create(dai.node.XLinkOut)
        xoutDepth.setStreamName("depth")
        stereo.depth.link(xoutDepth.input)

        xoutDepth = pipeline.create(dai.node.XLinkOut)
        xoutDepth.setStreamName("disp")
        stereo.disparity.link(xoutDepth.input)
        
        return pipeline, stereo

    def clip_outliers(self, features, k, percentile):
        whitened = whiten(features)
        (centroids, labels) = kmeans2(whitened, k, minit='points')
        std = np.std(features, axis=0)
        denormalized = list(centroids*std)
        # points array will be used to reach the index easily
        points = np.empty((0,len(features[0])), float)
        # distances will be used to calculate outliers
        distances = np.empty((0,len(features[0])), float)
        # getting points and distances
        for i, center_elem in enumerate(denormalized):
            # cdist is used to calculate the distance between center and other points
            distances = np.append(distances, cdist([center_elem],features[labels == i], 'euclidean')) 
            points = np.append(points, features[labels == i], axis=0)
        # Remove outliers
        new_data = points[np.where(distances <= np.percentile(distances, percentile))]
        std2 = np.std(new_data, axis=0)
        # Recalculate clusters
        whitened = whiten(new_data)
        (centroids, labels) = kmeans2(whitened, k, minit='points')
        # final_coords = []
        # for i, center_elem in enumerate(centroids):
        #     final_coords.append(np.mean(new_data[labels == i], axis=0))
        final_coords = centroids*std2
        return final_coords

    def find_raspberry_coordinate(self):
        """Find raspberry coordinate through stereo vision"""
        
        pipeline, stereo = self.set_camera_pipeline() 
        
        # Connect to device and start pipeline
        with dai.Device(pipeline) as device:
            # Output queue will be used to get the rgb frames from the output defined above
            qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
            depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
            dispQ = device.getOutputQueue(name="disp")

            hostSpatials = HostSpatialsCalc(device)
            text = TextHelper()
            hostSpatials.setDeltaRoi(self.delta)

            all_depth_coord = []
            picture_coord = []
            raspDetection = []
            rgb_frames = []
            depthMap = []
            sum_rasp = 0
            all_x = []
            all_y = []
            all_z = []

            while self.cameraOn > 0:
                # blocking call, will wait until a new data has arrived
                image  = qRgb.get().getCvFrame() 
                rasp_coords, _, found_rasps, _ = self.get_rasperries(image)

                depthFrame = depthQueue.get().getFrame() 
                depthFrame = depthFrame[self.y_init:self.y_init+self.h_adj, self.x_init:self.x_init+self.w_adj]

                # Get disparity frame for nicer depth visualization
                disp = dispQ.get().getFrame()
                disp = (disp * (255 / stereo.initialConfig.getMaxDisparity())).astype(np.uint8)
                disp = cv.applyColorMap(disp, cv.COLORMAP_JET)
                disp = disp[self.y_init:self.y_init+self.h_adj, self.x_init:self.x_init+self.w_adj]

                for raspberry in rasp_coords:
                    # Calculate spatial coordiantes from depth frame
                    (x, y) = (raspberry[0],raspberry[1])
                    spatials, centroid = hostSpatials.calc_spatials(depthFrame, (x,y)) # centroid == x/y in our case
                    
                    x_depth = spatials['x']
                    y_depth = spatials['y']
                    z_depth = spatials['z']

                    text.rectangle(disp, (x-self.delta, y-self.delta), (x+self.delta, y+self.delta))
                    text.putText(found_rasps, "X: " + ("{:.1f}mm".format(x_depth) if not math.isnan(x_depth) else "--"), (x + 10, y + 20))
                    text.putText(found_rasps, "Y: " + ("{:.1f}mm".format(y_depth) if not math.isnan(y_depth) else "--"), (x + 10, y + 35))
                    text.putText(found_rasps, "Z: " + ("{:.1f}mm".format(z_depth) if not math.isnan(z_depth) else "--"), (x + 10, y + 50))

                    if self.delay <= 0:
                        if (not math.isnan(x_depth)) and (not math.isnan(y_depth)) and (not math.isnan(z_depth)):
                            all_depth_coord.append((x_depth, y_depth, z_depth))   
                            picture_coord.append((x, y))
                            sum_rasp += len(rasp_coords)
                            all_x.append(x_depth)
                            all_y.append(y_depth)
                            all_z.append(z_depth)

                self.delay = self.delay - 1
                self.cameraOn = self.cameraOn - 1
                cv.imshow("detected circles", found_rasps)
                cv.imshow("depth", disp)
                # cv.imshow("reds", new_hue)
                depthMap.append(disp)
                raspDetection.append(found_rasps)
                rgb_frames.append(image)

                if cv.waitKey(1) == ord('q'):
                    break
        total_rasp = np.ceil(sum_rasp/len(all_depth_coord))
        all_coord = np.array(all_depth_coord)
        final_coords = self.clip_outliers(all_coord, int(total_rasp), self.percentile)
        # pic_coords = self.clip_outliers(picture_coord, int(total_rasp), self.percentile)
        self.save_video_data(depthMap, "disparity_map", (self.w_adj, self.h_adj))
        self.save_video_data(rgb_frames, "detected_raspberries.", (self.w_adj, self.h_adj))
        self.save_video_data(raspDetection, "rgb_frame", (self.w_rgb, self.h_rgb))
        return final_coords

    def choose_raspberry(self, raspberries):
        # Find the raspberry closest to the centre of the frame
        distances = []
        if raspberries.shape[0] > 1:
            for i in range(raspberries.shape[0]):
                distances.append(np.sqrt(raspberries[i][0]**2+raspberries[i][1]**2) )
            print('\n\n')
            print(distances)
            centre_rasp_indx = np.where(distances==np.min(distances))[0][0]
            centre_rasp = raspberries[centre_rasp_indx][:]
        else:
            centre_rasp = raspberries
        return centre_rasp, distances

    def cam_to_UR3(self, coord, R, p6):
        """Coordinate transformation from camera frame to UR3 frame"""
        coord = np.array([[coord[0]/1000],[coord[1]/1000],[coord[2]/1000]])
        pcam0 = R@self.pcam6
        pcam = np.array([[p6[0]+pcam0[0][0]],[p6[1]+pcam0[1][0]],[p6[2]+pcam0[2][0]]])
        return (R@(coord-self.z_offset))+pcam-self.robot_offset

    def save_video_data(self, data, file_name, size):
        os.chdir(self.directory)
        if os.path.isdir('Raspberry detection') is False:
            os.makedirs('Raspberry detection')
        os.chdir('Raspberry detection')
        dt_string = datetime.now().strftime("%d%m%Y_%H%M%S")
        os.makedirs(dt_string)
        os.chdir(dt_string)
        # Saves videos
        out = cv.VideoWriter(file_name+".avi",cv.VideoWriter_fourcc(*'DIVX'), 15, size)
        for i in range(len(data)):
            out.write(data[i])
        os.chdir('../../../')


    # def save_data(self, depth, rgb, raspDetection, coords):
    #         os.chdir(self.directory)
    #         if os.path.isdir('Raspberry detection') is False:
    #             os.makedirs('Raspberry detection')
    #         os.chdir('Raspberry detection')
    #         dt_string = datetime.now().strftime("%d%m%Y_%H%M%S")
    #         os.makedirs(dt_string)
    #         os.chdir(dt_string)
    #         # Saves videos
    #         out_depth = cv.VideoWriter('disparity_map.avi',cv.VideoWriter_fourcc(*'DIVX'), 15, (self.w_adj, self.h_adj))
    #         out_rasps = cv.VideoWriter('detected_raspberries.avi',cv.VideoWriter_fourcc(*'DIVX'), 15, (self.w_adj, self.h_adj))
    #         out_rgb = cv.VideoWriter('rgb_frame.avi',cv.VideoWriter_fourcc(*'DIVX'), 15, (self.w_rgb, self.h_rgb))
    #         for i in range(len(depth)):
    #             out_depth.write(depth[i])
    #         for i in range(len(rgb)):
    #             out_rgb.write(rgb[i])
    #         for i in range(len(raspDetection)):
    #             out_rasps.write(raspDetection[i])
    #         out_depth.release()
    #         out_rgb.release()
    #         out_rasps.release()
    #         # Writing coordinates to csv file
    #         coord_str = []
    #         with open("stereo_coordinates.csv", 'w') as csvfile:
    #             # creating a csv writer object
    #             csvwriter = csv.writer(csvfile)
                
    #             # writing the data rows
    #             coord_str.append(str(x) for x in coords)
    #             csvwriter.writerows(coord_str)
    #         os.chdir('../../../')
