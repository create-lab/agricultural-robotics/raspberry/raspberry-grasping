import cv2
import numpy as np
import json

def get_euclidian_dist(pt1, pt2):
    return np.sqrt((pt1[0] - pt2[0])**2 + (pt1[1] - pt2[1])**2)

# # Read image
# img = cv2.imread('plane.png')
 
def obtain_angle_on_img(img):
    # Create point matrix get coordinates of mouse click on image
    point_matrix = np.ones((5,2),int) * 10000

    def get_count(matrix):
        count = 5
        for i in range(5):
            if matrix[i][0] == 10000:
                count = i
                break
        return count

    def mousePoints(event,x,y,flags,params):
        cnt = get_count(point_matrix)
        # Left button mouse click event opencv
        if event == cv2.EVENT_LBUTTONDOWN:
            point_matrix[cnt] = x,y

    while True:
        point_counter = get_count(point_matrix)

        color = (0, 255, 0)
        for i in range(point_counter):
            cv2.circle(img,(point_matrix[i][0],point_matrix[i][1]),3,color,cv2.FILLED)

        if point_counter == 2:
            pts1 = np.copy(point_matrix[0:2, :])
            cv2.line(img, (pts1[0]), (pts1[1]), (255, 0, 0), thickness=2, lineType=8)
            midpoint1 = np.array([int((pts1[0][0] + pts1[1][0])/2), int((pts1[0][1] + pts1[1][1])/2)])

        if point_counter == 4:
            pts2 = np.copy(point_matrix[2:4, :])
            cv2.line(img, (pts2[0]), (pts2[1]), (255, 0, 0), thickness=2, lineType=8)
            midpoint2 = np.array([int((pts2[0][0] + pts2[1][0])/2), int((pts2[0][1] + pts2[1][1])/2)])

        if point_counter == 5:
            # for i in range(point_counter):
            #     cv2.circle(img,(point_matrix[i][0],point_matrix[i][1]),3,color,cv2.FILLED)

            berry = np.copy(point_matrix[4])

            for midpoint in [midpoint1, midpoint2]:
                cv2.circle(img,(midpoint[0],midpoint[1]),5,(255,255,0),cv2.FILLED)

            centre_point  = np.array([int((midpoint1[0] + midpoint2[0])/2), int((midpoint1[1] + midpoint2[1])/2)])
            
            cv2.circle(img,(centre_point[0],centre_point[1]),5,(255,255,255),cv2.FILLED)

            cv2.line(img, (midpoint1), (midpoint2), (255, 0, 0), thickness=2, lineType=8)

            print(midpoint1)
            print(midpoint2)

            if midpoint1[0] > midpoint2[0]:
                mp_left = midpoint2
                mp_right = midpoint1
            else:
                mp_left = midpoint2
                mp_right = midpoint2

            try:
                mp_slope = (mp_right[1] - mp_left[1]) / (mp_right[0] - mp_left[0])
            except:
                mp_slope = (mp_right[1] - mp_left[1]) / (mp_right[0] * 1.01 - mp_left[0])

            mp_intercept = mp_right[1] - mp_slope*mp_right[0]

            second_slope = -1/mp_slope
            second_intercept = berry[1] - second_slope*berry[0]

            print(mp_slope, second_slope)

            new_pt_x = (second_intercept - mp_intercept)/(mp_slope - second_slope)
            new_pt_y = second_slope * new_pt_x + second_intercept
            new_pt = np.array([int(new_pt_x), int(new_pt_y)])

            cv2.circle(img,(new_pt[0],new_pt[1]),5,(0,255,255),cv2.FILLED)

            h_dist = get_euclidian_dist(new_pt, centre_point)
            v_dist = get_euclidian_dist(new_pt, berry)

            finger_legnth = (get_euclidian_dist(pts1[0], pts1[1]) + get_euclidian_dist(pts2[0], pts2[1]))/2
            scalar = 26 / finger_legnth

            h_dist_true = h_dist * scalar
            v_dist_true = v_dist * scalar
            
            print("H:", h_dist_true)
            print("V:", v_dist_true)

            cv2.imshow("Original Image ", img)
            cv2.waitKey(10)

            input()
            break
            
        # Showing original image
        cv2.imshow("Original Image ", img)
        cv2.setMouseCallback("Original Image ", mousePoints)
        cv2.waitKey(10)

    cv2.destroyAllWindows()

    return [h_dist_true, v_dist_true]

def main():
    data = {}
    for i in range(12):
        try:
            img = cv2.imread(str(i+1) + '.jpg')

            if i == 6 or i == 7 or i == 8:
                # grab the dimensions of the image and calculate the center of the
                # image
                (h, w) = img.shape[:2]
                (cX, cY) = (w // 2, h // 2)
                # rotate our image by 45 degrees around the center of the image
                M = cv2.getRotationMatrix2D((cX, cY), 45, 1.0)
                img = cv2.warpAffine(img, M, (w, h))

            offset = obtain_angle_on_img(img)
            
            data[i] = offset
        except:
            break

    # Serializing json
    json_object = json.dumps(data, indent=4)
    
    # Writing to sample.json
    with open("offset5.json", "w") as outfile:
        outfile.write(json_object)

if __name__ == "__main__":
    main()