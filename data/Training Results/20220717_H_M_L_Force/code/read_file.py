import numpy as np
import csv

def read_csv_file(filename):
    rows = []
    # reading csv file
    with open(filename, 'r') as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)

        # extracting each data row one by one
        for row in csvreader:
            rows.append(row)
    return rows


def find_fails(fail_str, find_fd, rows):
    index = []
    for i, row in enumerate(rows):
        if len(row) > 0:
            compare_str1 = row[0][0:len(fail_str)]
            compare_str2 = row[0][0:len(find_fd)]
            if compare_str1 == fail_str:
                index.append(i)
            elif compare_str2 == find_fd:
                if rows[i+1][0][0] == 'C':
                    index.append(i)
    return index


def read_loss_error(find_vector, fail_index, rows):
    prev_pull = 0
    prev_rel = 0
    pull = 0
    rel = 0
    vector = []
    for i, row in enumerate(rows):
        if i not in fail_index:
            if len(row) > 0:
                compare_str = [row[0][0:len(find_vector[0])], row[0][0:len(find_vector[1])]]
                if compare_str[0] == find_vector[0]:
                    pull = float(row[0][len(find_vector[0])::])
                elif compare_str[1] == find_vector[1]:
                    rel = float(row[0][len(find_vector[1])::])
                if prev_pull != pull and prev_rel != rel:
                    vector.append([pull, rel])
                    prev_rel = rel
                    prev_pull = pull
    return vector


def read_pressure_pulling_force(find_vector, fail_index, rows):
    found = False
    vector = []
    array = []
    for i, row in enumerate(rows):
        if i not in fail_index:
            if len(row) > 0:
                compare_str = row[0][0:len(find_vector)]
                if compare_str == find_vector:
                    array.append(float(row[0][len(find_vector)::]))
                    found = True
                    for k in range(1, len(row)):
                        if found and row[k][-1] != ']':
                            array.append(float(row[k]))
                        elif found and row[k][-1] == ']':
                            array.append(float(row[k][0:len(row[k])-1]))
                            vector.append(array)
                            array = []
                            found = False
    return vector


def read_Fd(find_vector, fail_index, rows):
    vector = []
    for i, row in enumerate(rows):
            if i not in fail_index:
                if len(row) > 0:
                    compare_str = row[0][0:len(find_vector)]
                    if compare_str == find_vector:
                        white1 = row[0][len(find_vector)::].index(' ')
                        fd1 = float(row[0][len(find_vector):len(find_vector)+white1])
                        k = len(find_vector)+white1+1
                        fd2 = fd1
                        while fd2 == fd1:
                            while row[0][k] == ' ':
                                k += 1
                                # print(i, k, row[0][k], row[0][k] == ' ')
                                if k == len(row[0]):
                                    k = 0
                                    i += 1
                                    row = rows[i]
                            if ' ' in row[0][k::]:
                                white2 = row[0][k::].index(' ')
                                fd2 = float(row[0][k:k+white2])
                                # print(fd2)
                            else:
                                fd2 = float(row[0][k::])
                                # print(fd2)
                            k = k+white2+1
                        vector.append([fd1, fd2])
    return vector