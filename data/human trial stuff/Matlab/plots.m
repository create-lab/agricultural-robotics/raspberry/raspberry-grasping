for i = 1:100
    figure
    subplot(2,1,1)
    plot(time{i},pressure{i})
    xlabel('Time (s)')
    ylabel('Raspberry measurement (g)')
    subplot(2,1,2)
    plot(time{i},pulling{i})    
    xlabel('Time (s)')
    ylabel('Loadcell measurement (g)')
end

