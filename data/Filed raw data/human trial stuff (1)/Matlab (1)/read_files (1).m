files = {'human trials 2 - pressure.txt', 'human trials 2 - pulling.txt', 'human trials 2 - time series.txt'};

for i = 1:size(files,2)
    file = files{i};
   
    %Abertura do ficheiro e permissão para este ser lido
    ficheiro_lido = fopen(file, 'rt');
    
    %verificar a abertura do ficheiro
    if ficheiro_lido == -1
        fprintf (2,'Erro na abertura do ficheiro\n a matriz numeros está vazia \n');

    else
        j = 1;
        k = 1;
        data = {};
        flag = 0;
        while ~feof (ficheiro_lido)
            linha = fgetl(ficheiro_lido);
            if isnan(str2double(linha)) == 0 
                vector(j) = str2double(linha);
                j = j + 1;
            else
                flag = flag + 1;
                if flag == 5
                    data{k} = vector;
                    k = k + 1;
                    flag = 0;
                    vector = 0;
                    j = 1;                   
                end
            end
        end
        %Fecho do ficheiro
        ficheiro_fechado = fclose(ficheiro_lido);

        %Verificação se o ficheiro foi fechado devidamente
        if ficheiro_fechado == -1
            fprintf (2,'O ficheiro não foi fechado devidamente\n');
        end
        if i == 1
            pressure = data;
        elseif i == 2
            pulling = data;
        elseif i == 3
            time = data;
        end 
    end
end
